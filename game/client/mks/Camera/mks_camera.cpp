#include "cbase.h"

#include "c_sdk_player.h"
#include "in_buttons.h"

#include "mks/Camera/mks_camera.h"
#include "mks/Player/c_mks_player.h"
#include "mks/Kart/c_mks_kart.h"
#include "mks/Math/Quaternion.h"

ConVar mks_cam_dist( "mks_cam_dist", "155" );
ConVar mks_cam_up( "mks_cam_up", "70" );
ConVar mks_cam_pitch( "mks_cam_pitch", "0" );
ConVar mks_cam_yaw( "mks_cam_yaw", "0" );
ConVar mks_cam_convergespeed( "mks_cam_convergespeed", "250" );

MKSCamera::MKSCamera()
{
	m_vecCurrentUp.Init(0,0,1);
	m_vecTargetUp.Init(0,0,1);
	m_vecCurrentForward.Init(1,0,0);
	m_vecTargetForward.Init(1,0,0);
	m_qCurrent.Init();
	m_qTarget.Init();

	m_pTarget = NULL;

	m_vecOrigin.Init();
	m_angAngles.Init();
}

MKSCamera::~MKSCamera()
{
	
}

void MKSCamera::Release( void )
{
	m_pTarget = NULL;
	delete this;
}

void MKSCamera::UpdateCamera( bool bTeleport )
{
	if(!m_pTarget)
		return;

	matrix3x4_t matKart = m_pTarget->EntityToWorldTransform();


	Vector axis[3];
	Vector vOrig, vRight;

	
	MatrixVectors( matKart, &m_vecTargetForward, &vRight, &m_vecTargetUp );
	
	vOrig = m_pTarget->GetAbsOrigin();

	
	m_qTarget.FromRotationMatrix( matKart );

	if(bTeleport)
		m_flSlerp = 1.0f;
	else
		m_flSlerp = CalculateSlerp( m_qCurrent, m_qTarget );

	if(m_flSlerp < 0.0f)
		m_flSlerp = 0.0f;

	
	if(m_flSlerp < 1.0f)
	{
		m_qCurrent.Slerp( m_qCurrent, m_qTarget, m_flSlerp );
	}
	else
	{
		m_qCurrent = m_qTarget;
	}

	Quat temp = m_qCurrent;
	
	yaw( mks_cam_yaw.GetFloat() );
	pitch( mks_cam_pitch.GetFloat() );

	m_qCurrent.ToAxes( axis );
	m_vecCurrentUp = axis[2];
	m_vecCurrentForward = axis[0];

	m_vecOrigin = vOrig + (m_vecCurrentUp * mks_cam_up.GetFloat()) + (m_vecCurrentForward * -mks_cam_dist.GetFloat());
	m_qCurrent.GetAngles( m_angAngles );
	m_qCurrent = temp;
}

float MKSCamera::CalculateSlerp( const Quat &current, const Quat &target )
{
	float dotprod = current.dot_prod( target );//DotProduct( current, target );
	if(dotprod > 1.0f)
		dotprod = 1.0f;
	else if(dotprod < -1.0f)
		dotprod = -1.0f;
	//else
		dotprod = dotprod * -1.0f;

	Vector curAxis = current * Vector(1,0,0);
	Vector tarAxis = target * Vector(1,0,0);
	dotprod = DotProduct( curAxis, tarAxis );

	float angle = RAD2DEG( acos( dotprod ) );
	angle = fabs(angle);

	float lerp = 0.0f;
	float anglePerSecond = mks_cam_convergespeed.GetFloat() * gpGlobals->frametime;
	//engine->Con_NPrintf( 23, "Angle = %f DotProduct = %f", angle, dotprod );
	if(angle != 0.0f )
	{
		if(angle > 45.0f)
		{
			lerp = 1.0f - (45.0f / angle);
		}
		else
		{
			float temp = (angle / 45.0f);
			lerp = (anglePerSecond / angle) * temp;
		}
		
		//lerp *= gpGlobals->frametime;

		//lerp *= lerp;

		if(lerp > 1.0f)
		{
			engine->Con_NPrintf( 23, "Angle = %f Lerp = %f", angle, lerp );
			lerp = 1.0f;
			
		}
	}
	return lerp;
}

void MKSCamera::roll(const float& angle)
{
    // Rotate around local Z axis
    Vector zAxis = m_qCurrent * Vector(1,0,0);
    rotate(zAxis, angle);
}

//-----------------------------------------------------------------------
void MKSCamera::yaw(const float& angle)
{
    Vector yAxis = m_qCurrent * Vector(0,0,1);
    rotate(yAxis, angle);
}

//-----------------------------------------------------------------------
void MKSCamera::pitch(const float& angle)
{
    // Rotate around local X axis
    Vector xAxis = m_qCurrent * Vector(0,1,0);
    rotate(xAxis, angle);
}

//-----------------------------------------------------------------------
void MKSCamera::rotate(const Vector& axis, const float& angle)
{
    Quat q;
    q.AxisAngle(axis, angle);
    rotate(q);
}

//-----------------------------------------------------------------------
void MKSCamera::rotate(const Quat& q)
{
    // Note the order of the mult, i.e. q comes after
    m_qCurrent = q * m_qCurrent;
}
