#ifndef MKS_CAMERA
#define MKS_CAMERA


#include "mks/Math/Quaternion.h"


class MKSCamera
{
	
public:
	MKSCamera();
	~MKSCamera();

	void UpdateCamera( bool bTeleport );

	void Release( void );

	void GetCameraInfo( Vector &origin, QAngle &angles ) { origin = m_vecOrigin; angles = m_angAngles; };
	float CalculateSlerp( const Quat &current, const Quat &target );

	//Rotate about our current axes.
	void roll(const float& angle); //degrees
	void yaw(const float& angle); //degrees
	void pitch(const float& angle); //degrees

	//Rotate according to a custom axis or quaternion.
	void rotate(const Vector& axis, const float& angle); //degrees
	void rotate(const Quat& q);

	void SetTarget( C_BaseEntity *pTarget ) { m_pTarget = pTarget;	};
	C_BaseEntity *GetTarget()				{ return m_pTarget;		};

private:
	C_BaseEntity *m_pTarget;

	Vector	m_vecOrigin;
	QAngle	m_angAngles;

	Quat	m_qCurrent;
	Quat	m_qTarget;

	Vector	m_vecCurrentUp;
	Vector	m_vecTargetUp;

	Vector	m_vecCurrentForward;
	Vector	m_vecTargetForward;
	float	m_flSlerp;
};

#endif //MKS_CAMERA