//======================================================================\\
// MKS-OneEyed: Creates a itembox on the mapper defined locations.
// TODO: Random weapons spawning.
//======================================================================//
#include "cbase.h"



class C_MKSItemBox : public C_BaseAnimating
{
public:
	DECLARE_CLASS( C_MKSItemBox, C_BaseAnimating );
	DECLARE_CLIENTCLASS();
	

	
	void Spawn( void );
	void Touch(CBaseEntity *pOther);
	int DrawModel(int flags);
	virtual void ClientThink();
	virtual void PostDataUpdate( DataUpdateType_t type );

public:
	virtual const QAngle& GetRenderAngles( void );

	float m_flOffset;
	Vector m_vecOriginalOrigin;
	bool m_bGoingUp;
	bool m_bActive;

	
	
};


LINK_ENTITY_TO_CLASS( mks_itembox, C_MKSItemBox );

IMPLEMENT_CLIENTCLASS_DT(C_MKSItemBox, DT_MKSItemBox, CMKSItemBox)
	RecvPropBool(RECVINFO(m_bActive)),
END_RECV_TABLE()

void C_MKSItemBox::Spawn()
{
	m_flOffset = 0.0f;
	m_bGoingUp = false;
	// = GetAbsOrigin();
}

const QAngle &C_MKSItemBox::GetRenderAngles()
{
	const float turnSpeed = 30.0f * gpGlobals->frametime;
	QAngle angle = GetAbsAngles();
	angle.y += turnSpeed;
	SetAbsAngles( angle );
	return GetAbsAngles();
}

void C_MKSItemBox::ClientThink()
{
	SetCycle( 0.0f );

	

//	const float maxUp = 10.0f;
//	const float maxDown = -5.0f;
	//Vector curOrigin = GetAbsOrigin();

	if(m_vecOriginalOrigin.IsZero())
		m_vecOriginalOrigin = GetAbsOrigin();

/*	if(m_bGoingUp)//m_flOffset <= maxUp)
		ConvergeOrigin( maxUp, m_flOffset, 100.0f );
	else
		ConvergeOrigin( maxDown, m_flOffset, 50.0f );*/
	
	if(m_flOffset >= 10.0)
		m_bGoingUp = false;
	else if(m_flOffset <= -5.0)
		m_bGoingUp = true;

	SetAbsOrigin( m_vecOriginalOrigin + Vector( 0, 0, m_flOffset ) );

	
	
	//SetAbsAngles( angle );
	/*
	SetSequence( LookupSequence( "Idle" ) );
	float cycle = GetCycle();
	if(cycle >= 1.0f)
		cycle = 0.0f;

	SetCycle( cycle );
	SetPlaybackRate( 1.0 );
	FrameAdvance( gpGlobals->frametime );
	
	*/
	SetNextClientThink( gpGlobals->curtime + 0.05f );
}

void C_MKSItemBox::PostDataUpdate( DataUpdateType_t type )
{
	BaseClass::PostDataUpdate( type );
	
	if( type == DATA_UPDATE_CREATED )
	{
		
		float rand = random->RandomFloat( 0.0f, 1.0f );
		SetCycle( rand );
		SetNextClientThink( gpGlobals->curtime + 0.05f );
		SetTouch( &C_MKSItemBox::Touch );
	}
}

int C_MKSItemBox::DrawModel(int flags)
{
	if (m_bActive){
		return BaseClass::DrawModel(flags);
	}
	return 0;
}

void C_MKSItemBox::Touch(CBaseEntity *pOther)
{
	if(GetRenderColor().a == 0)
		return;

	//FX_ItemBox( m_vecOriginalOrigin + Vector( 0, 0, m_flOffset ), Vector(0,0,1), 5.0f );
	/*
	
	if(pOther->IsPlayer() && m_flNextReadyTime == 0.0)
	{
		CSDKPlayer *pPlayer = ToSDKPlayer( pOther );
		if(pPlayer)
		{
			if(pPlayer->IsBabomb())
				return;

			if(pPlayer->m_iItemSpin == -1 && pPlayer->m_iSlotWeapon == WEAPON_LAST)
			{
				pPlayer->ActivateSpinner();
			}
			
			SetRenderMode( kRenderTransAlpha );
			//m_nRenderFX = kRenderFxNone;
			SetRenderColorA( 0 );
			//SetEFlags( EF_NODRAW );
			//SetSolid( SOLID_NONE );
			m_flNextReadyTime = gpGlobals->curtime + 5.0;
		}
	}
	*/
}
