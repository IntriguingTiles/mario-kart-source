
#include "cbase.h"
#include "mks/Kart/c_mks_clientkart.h"
#include "mks/Player/c_mks_player.h"
#include "mks/Kart/c_mks_engine.h"
#include "mks/Kart/c_mks_wheel.h"
#include "mks/Kart/c_mks_kartmovement.h"

#include "mks/Kart/c_mks_kart.h"

#include "mks/Rules/mks_cvar_settings.h"
#include "c_gib.h"
#include "vcollide_parse.h"

/*
bool C_MKSClientKart::Initialize()
{
	if ( InitializeAsClientEntity( STRING(GetModelName()), RENDER_GROUP_OPAQUE_ENTITY ) == false )
	{
		return false;
	}

	const model_t *mod = GetModel();
	if ( mod )
	{
		Vector mins, maxs;
		modelinfo->GetModelBounds( mod, mins, maxs );
		SetCollisionBounds( mins, maxs );
	}

	solid_t tmpSolid;

	// Create the object in the physics system

	if ( !PhysModelParseSolid( tmpSolid, this, GetModelIndex() ) )
	{
		DevMsg("C_PhysPropClientside::Initialize: PhysModelParseSolid failed for entity %i.\n", GetModelIndex() );
		return false;
	}
	else
	{
		m_pPhysicsObject = VPhysicsInitNormal( SOLID_VPHYSICS, 0, m_spawnflags & SF_PHYSPROP_START_ASLEEP, &tmpSolid );
	
		if ( !m_pPhysicsObject )
		{
			// failed to create a physics object
		DevMsg(" C_PhysPropClientside::Initialize: VPhysicsInitNormal() failed for %s.\n", STRING(GetModelName()) );
			return false;
		}
	}

	// We want touch calls when we hit the world
	unsigned int flags = VPhysicsGetObject()->GetCallbackFlags();
	VPhysicsGetObject()->SetCallbackFlags( flags | CALLBACK_GLOBAL_TOUCH_STATIC );

	if ( m_spawnflags & SF_PHYSPROP_MOTIONDISABLED )
	{
		m_pPhysicsObject->EnableMotion( false );
	}
		
	Spawn(); // loads breakable & prop data

	if ( m_iPhysicsMode == PHYSICS_MULTIPLAYER_AUTODETECT )
	{
		m_iPhysicsMode = GetAutoMultiplayerPhysicsMode( 
			CollisionProp()->OBBSize(), m_pPhysicsObject->GetMass() );
	}

	if 	( m_spawnflags & SF_PHYSPROP_FORCE_SERVER_SIDE )
	{
		// forced to be server-side by map maker
		return false;
	}
		

	
	if ( m_iPhysicsMode != PHYSICS_MULTIPLAYER_CLIENTSIDE )
	{
		// spawn only clientside entities
		return false;
	}
	else 
	{
		if ( engine->IsInEditMode() )
		{
			// don't spawn in map edit mode
			return false;
		}
	}

	if ( m_fadeMinDist < 0 )
	{
		// start fading out at 75% of r_propsmaxdist
		m_fadeMaxDist = r_propsmaxdist.GetFloat();
		m_fadeMinDist = r_propsmaxdist.GetFloat() * 0.75f;
	}

	// player can push it away
	SetCollisionGroup( COLLISION_GROUP_PUSHAWAY );

	UpdatePartitionListEntry();

	CollisionProp()->UpdatePartition();

	SetBlocksLOS( false ); // this should be a small object

	// Set up shadows; do it here so that objects can change shadowcasting state
	CreateShadow();

	UpdateVisibility();

	SetNextClientThink( CLIENT_THINK_NEVER );

	return true;
}

void C_MKSClientKart::Spawn()
{
	// Initialize damage modifiers. Must be done before baseclass spawn.
	m_flDmgModBullet = 1.0;
	m_flDmgModClub = 1.0;
	m_flDmgModExplosive = 1.0;

	BaseClass::Spawn();

	// we don't really precache models here, just checking how many we have:
	m_iNumBreakableChunks = PropBreakablePrecacheAll( GetModelIndex() );

	ParsePropData();

	// If we have no custom breakable chunks, see if we're breaking into generic ones
	if ( !m_iNumBreakableChunks )
	{
		if ( GetBreakableModel() != NULL_STRING && GetBreakableCount() )
		{
			m_iNumBreakableChunks = GetBreakableCount();
		}
	}

	// Setup takedamage based upon the health we parsed earlier
	if ( m_iHealth == 0 )
	{
		m_takedamage = DAMAGE_NO;
	}
	else
	{
		m_takedamage = DAMAGE_YES;
	}
}

*/

//LINK_ENTITY_TO_CLASS( mks_clientkart, C_MKSClientKart );

C_MKSClientKart::C_MKSClientKart()
{
	m_flKartVelocity = 0.0f;
	m_flKartSpeed = 0.0f;
	m_flMaxKartSpeed = 0.0f;
	m_flThrottle = 0.0f;
	m_flBrake = 0.0f;
	m_flSteering = 0.0f;
	m_bSliding = false;
	m_flFriction = 0.0f; //Strong Friction

	m_pDriver = NULL;
	m_pMovement = NULL;
	m_pEngine = NULL;
	SetGravityDirection( Vector(0,0,-1) );

	ClearAddVelocity();

	m_iWheelGroundCount = 0;
	m_iWheelCount = 0;

	m_vAddVelocity = Vector(0,0,0);
	m_angVelocity = AngularImpulse(0,0,0);

	m_vWorldOrigin = Vector(0,0,0);
	m_vForward = Vector(0,0,0);
	m_vRight = Vector(0,0,0);
	m_vUp = Vector(0,0,0);
}


C_MKSClientKart::~C_MKSClientKart()
{
}

//-----------------------------------------------------
// C_MKSClientKart::PhysicsSolidMaskForEntity
//
// returns: Solid Mask
//
// Description: The Solid Mask of entity.
//-----------------------------------------------------
unsigned int C_MKSClientKart::PhysicsSolidMaskForEntity( void ) const
{
	return 0;// MASK_PLAYERSOLID;
}

//-----------------------------------------------------
// C_MKSClientKart::GetDriver
//
// returns: Current Driver
//
// Description: Gets the pointer to the current player driver.
//-----------------------------------------------------
CMKSPlayer *C_MKSClientKart::GetDriver(void)
{
	return m_pDriver;
}

//-----------------------------------------------------
// C_MKSClientKart::SetDriver
//
// param1: Player who will drive.
//
// Description: Saves the player pointer to the driver seat.
//-----------------------------------------------------
void C_MKSClientKart::SetDriver( CMKSPlayer *mksPlayer )
{
	m_pDriver = mksPlayer;
}

/*
//-----------------------------------------------------
// C_MKSClientKart::SetProfile
//
// param1: Kart Profile Name (found in the kart script files)
//
// Description: Copy the profile data into our MKSPlayerProfile struct.
//-----------------------------------------------------
void C_MKSClientKart::SetProfile( const char *szKartName )
{
	m_pProfile.Copy( g_KartProfiles->GetProfile( szKartName ) );
}
*/

//-----------------------------------------------------
//C_MKSClientKart::Precache
//
//Description: Precache Kart Model from Profile
//-----------------------------------------------------
void C_MKSClientKart::Precache( void )
{
	
	/*
	int a;
	int iProfileCount = g_KartProfiles->m_Profiles.Count();
	MKSPlayerProfile *pProfile = NULL;

	for( a=0; a<iProfileCount; a++ )
	{
		pProfile = g_KartProfiles->m_Profiles[a];
		if( !pProfile )
			continue;
	
		//const char *szModel = pProfile->szModel; 
		//PrecacheModel( szModel );
		//SetModel( szModel );

		GetProfileIndexes( pProfile );
	}
	*/
	BaseClass::Precache();
}


//-----------------------------------------------------
// C_MKSClientKart::GetProfileIndexes
//
// param1: Profile from kart scripts
//
// Description: Update the internal values
//-----------------------------------------------------
void C_MKSClientKart::GetProfileIndexes( MKSPlayerProfile *pProfile )
{
	int b;
	int iWheelCount = pProfile->iWheelCount;
	for( b=0; b<iWheelCount; b++ )
	{
		const char *szAttachment = pProfile->szWheelAttachment[b];
		int iAttach = LookupAttachment( szAttachment );
		pProfile->iWheelAttachment.Set( b, iAttach );

		const char *szSuspension = pProfile->szWheelSuspension[b];
		int iPose = LookupPoseParameter( szSuspension );
		pProfile->iWheelSuspension.Set( b, iPose );
	}
}


//-----------------------------------------------------
// C_MKSClientKart::Spawn
//
// Description: Called when player spawns, setup kart.
//-----------------------------------------------------
void C_MKSClientKart::Spawn( void )
{
	Precache();
}

void C_MKSClientKart::SpawnClientEntity( void )
{
}


//-----------------------------------------------------
// C_MKSClientKart::Run
//
// Description: Process all moving parts
//-----------------------------------------------------
void C_MKSClientKart::Update( void )
{
	UpdateValues();

	//Process movement engine
	C_MKSEngine *mksEngine = GetEngine();
	mksEngine->Update();

	//Process all tires.
	CMKSWheel *mksWheel = NULL;
	int iTireCount = m_pWheels.Count();
	int a;
	for( a=0; a<iTireCount; a++ )
	{
		mksWheel = m_pWheels[a];
		if( mksWheel == NULL )
			continue;
		mksWheel->Update();
	}

	CalculateWheelData();
}


//-----------------------------------------------------
// C_MKSClientKart::CalculateWheelData
//
// Description: Get the average of wheel values (Ground Normal, Velocity, etc.)
//-----------------------------------------------------
void C_MKSClientKart::CalculateWheelData( void )
{
	Vector vNewGravity = Vector(0,0,1);
	Vector vCurrentGravity = GetGravityDirection();

	CMKSWheel *mksWheel = NULL;
	bool bIsOnGround = false;

	int iWheelCount = m_pWheels.Count();
	int iWheelGroundCount = 0;
	int a;
	for( a=0; a<iWheelCount; a++ )
	{
		mksWheel = GetWheel(a);
		if( mksWheel == NULL )
			continue;
		
		bIsOnGround = mksWheel->IsOnGround();

		vNewGravity += mksWheel->GetGroundNormal();

		if( bIsOnGround )
		{
			
			iWheelGroundCount++;
		}
	}

	SetWheelGroundCount( iWheelGroundCount );

	vNewGravity *= 100.0f;
	VectorNormalize( vNewGravity );
	//SetGravityDirection( -vNewGravity );
}


//-----------------------------------------------------
// C_MKSClientKart::Update
//
// Description: Update the internal values
//-----------------------------------------------------
void C_MKSClientKart::UpdateValues( void )
{
	IPhysicsObject *pKart = VPhysicsGetObject();

	pKart->GetPositionMatrix( &m_vMatWorld );
	MatrixVectors( m_vMatWorld, &m_vForward, &m_vRight, &m_vUp );

	Vector vOrigin = GetAbsOrigin();
	MatrixGetColumn( m_vMatWorld, 3, vOrigin );

	Vector vVelocity;
	AngularImpulse angVelocity;
	pKart->GetVelocity( &vVelocity, &angVelocity );

	float flKartVelocity = vVelocity.Length();
	SetKartVelocity( flKartVelocity );
	SetAbsVelocity( vVelocity );

	SetWorldOrigin( vOrigin );
}

//-----------------------------------------------------
// C_MKSClientKart::Drive
//
// Description: Send the player input to the kart engine.
//-----------------------------------------------------
void C_MKSClientKart::Drive( void )
{
	CMKSPlayer *mksPlayer = GetDriver();
	CUserCmd *pCmd = mksPlayer->GetCurrentUserCmd();
	GetEngine()->Steering( pCmd );
	GetEngine()->Gas( pCmd );
	GetEngine()->Brake( pCmd );
}

/*
//-----------------------------------------------------
// C_MKSClientKart::VPhysicsCollision
//
// Description: Detects Physics Collisions
//-----------------------------------------------------
void C_MKSClientKart::VPhysicsCollision( int index, gamevcollisionevent_t *pEvent )
{
	int otherIndex = !index;
	CBaseEntity *pOther = pEvent->pEntities[otherIndex];
	if ( pOther->IsPlayer() )
	{
		// Pushables don't take damage from impacts with the player
		// We call all the way back to the baseclass to get the physics effects.
		//C_BaseEntity::VPhysicsCollision( index, pEvent );
		return;
	}

	Vector vNormal;
	Vector vPos = GetWorldOrigin();
	Vector vPoint;

	pEvent->pInternalData->GetSurfaceNormal( vNormal );
	pEvent->pInternalData->GetContactPoint( vPoint );

	Vector vDir = vPoint - vPos;
	VectorNormalize( vDir );
	float flDiff = DotProduct( vDir, vNormal );
	if( flDiff > 0.0f )
		vNormal *= -1;

	float flDot = DotProduct( GetGravityDirection(), vNormal );
	float flAbsDot = fabs(flDot);
	if( flAbsDot < 0.7f )
	{
		IPhysicsObject *pKart = VPhysicsGetObject();
		Vector vForce;
		pEvent->pInternalData->GetContactSpeed( vForce );
		float flSpeed = vForce.Length();
		pKart->ApplyForceCenter( vNormal * flSpeed * mks_wall_bounce.GetFloat() );
		SetKartSpeed( GetKartSpeed() * 0.5f );
	}

	//BaseClass::VPhysicsCollision( index, pEvent );
}
*/

//-----------------------------------------------------
// C_MKSClientKart::InitProfile
//
// Description: Download the selected kart profile.
//-----------------------------------------------------
bool C_MKSClientKart::InitProfile( void )
{
//	CMKSPlayer *mksPlayer = GetDriver();

//	const char* szKartName = mksPlayer->GetKartName();
	SetServerKart( GetDriver()->GetServerKart() );

	SetProfile( &GetServerKart()->m_pProfile );

	//SetModel( m_pProfile.szModel );

	GetProfileIndexes( m_pProfile );

	m_iWheelCount = m_pProfile->iWheelCount;
	return true;
}


//-----------------------------------------------------
// C_MKSClientKart::InitializeKart
//
// Description: Setup any extra kart settings.
//-----------------------------------------------------
bool C_MKSClientKart::InitializeKart( void )
{	
	InitializePhysics();
	SetPhysicsParams();

	m_pPhysicsObject->Wake();

	//Build the kart engine.
	if( !GetEngine() )
	{
		C_MKSEngine *pEngine = C_MKSEngine::CreateEngine( this );
		if( pEngine == NULL )
			return false;
		SetEngine( pEngine );
	}

	//Build the kart wheels.
	CMKSWheel *pWheel = NULL;
	int a;
	for( a=0; a<m_iWheelCount; a++ )
	{
		pWheel = NULL;

		if( m_pWheels.Count() >= m_iWheelCount )
			pWheel = GetWheel(a);

		if( !pWheel )
		{
			pWheel = CMKSWheel::CreateWheel( this );
			if(!pWheel)
				continue;
		}
		pWheel->Init( m_pProfile );
	}	

	if( !m_pMovement )
	{
		//Build the kart movement controller
		m_pMovement = C_MKSKartMovement::CreateKartMovement( this );
		if( m_pMovement == NULL )
			return false;
	}
	return true;
}


//-----------------------------------------------------
// C_MKSClientKart::InitializePhysics
//
// Description: Create the physics mesh of our model.
//-----------------------------------------------------
bool C_MKSClientKart::InitializePhysics( void )
{

	//bool bPassed = BaseClass::InitializePhysics();

	//Get the Min/Max Vectors of the current model.
	CreateMinMax();
	
	// create a normal physics object
	m_pPhysicsObject = PhysModelCreateOBB( this, m_vCubeMins, m_vCubeMaxs, GetAbsOrigin(), GetAbsAngles(), false );
	if ( !m_pPhysicsObject )
	{
		// failed to create a physics object
		Warning(" CMKSCube::CreateCube: PhysModelCreateOBB() failed for %s.\n", STRING(GetModelName()) );
		return false;
	}

	// We want touch calls when we hit the world
	unsigned int flags = m_pPhysicsObject->GetCallbackFlags();
	m_pPhysicsObject->SetCallbackFlags( flags | CALLBACK_GLOBAL_TOUCH_STATIC | CALLBACK_GLOBAL_TOUCH | CALLBACK_SHADOW_COLLISION | CALLBACK_GLOBAL_COLLIDE_STATIC);

	SetCollisionGroup( COLLISION_GROUP_MKS_KART );

	return true;
}


//-----------------------------------------------------
// C_MKSClientKart::CreateMinMax
//
// Description: Calculate the Min/Max of our karts model.
//-----------------------------------------------------
void C_MKSClientKart::CreateMinMax( void )
{
	m_vCubeMins = GetServerKart()->m_vCubeMins;
	m_vCubeMaxs = GetServerKart()->m_vCubeMaxs;
	/*
	SetAbsOrigin( vec3_origin );
	SetAbsAngles( vec3_angle );

	InvalidateBoneCache();

	//Get the Min/Max of model
	const model_t *mod = GetModel();
	if ( !mod )
	{
		Warning("CMKSCube::CreateCube: GetModel failed for entity %i.\n", GetModelIndex() );
		return;
	}

	Vector mins, maxs;
	modelinfo->GetModelBounds( mod, mins, maxs );
	
	int iFrontRight = LookupAttachment( m_pProfile.szFrontLeft );
	int iRearRight = LookupAttachment( m_pProfile.szRearRight );

	Vector vFrontRight, vRearLeft;
	QAngle dummy;
	float flHeight = 25.0f;

	GetAttachmentLocal( iFrontRight, vFrontRight, dummy );
	GetAttachmentLocal( iRearRight, vRearLeft, dummy );

	mins = vRearLeft;

	maxs = vFrontRight;
	maxs.z = vFrontRight.z + flHeight;

	SetCollisionBounds( Vector(-0.5, -0.5, -0.5), Vector(0.5, 0.5, 0.5) );

	int iWheelCount = m_pProfile.iWheelCount;
	int a;
	float flMaxWheelRadius = 9999.9f;

	for( a=0; a<iWheelCount; a++ )
	{
		if( m_pProfile.flWheelRadius[a] < flMaxWheelRadius )
			flMaxWheelRadius = m_pProfile.flWheelRadius[a];
	}

	Vector vMinsDir = mins;
	Vector vMaxsDir = maxs;

	vMinsDir.z = 0;
	vMaxsDir.z = 0;

	VectorNormalize( vMinsDir );
	VectorNormalize( vMaxsDir );

	mins += vMinsDir * flMaxWheelRadius;
	maxs += vMaxsDir * flMaxWheelRadius;

	mins.z += -1.5f;

	m_vCubeMins = mins;
	m_vCubeMaxs = maxs;
	*/
}


//-----------------------------------------------------
// C_MKSClientKart::SetPhysicsParams
//
// Description: Load the settings of our physics kart.
//-----------------------------------------------------
void C_MKSClientKart::SetPhysicsParams( void )
{
	IPhysicsObject *pCube = VPhysicsGetObject();
	if(!pCube)
		return;

	MKSPlayerProfile *pProfile = GetProfile();

	float damping = m_flDamping = pProfile->flDamp;
	float rotdamping = m_flRotDamping = pProfile->flRotDamp;

	float drag = m_flDrag = pProfile->flDragCo;
	float rotdrag = m_flRotDrag = pProfile->flRotDrag;

	m_flMass = pProfile->flMass;
	m_flInertiaX = pProfile->vInertiaX;
	m_flInertiaY = pProfile->vInertiaY;
	m_flInertiaZ = pProfile->vInertiaZ;

	pCube->SetDamping( &damping, &rotdamping );
	pCube->SetDragCoefficient( &drag, &rotdrag );
	pCube->SetInertia( Vector( m_flInertiaX, m_flInertiaY, m_flInertiaZ ) );
	pCube->SetMass( m_flMass );

	pCube->EnableGravity( true );
	unsigned int contents = CONTENTS_PLAYERCLIP | CONTENTS_SOLID | CONTENTS_MOVEABLE;
	pCube->SetContents( contents );
	pCube->EnableCollisions( true );

	float flFriction = mks_friction_default.GetFloat();
	SetWheelFriction( flFriction );
}


//-----------------------------------------------------
// C_MKSClientKart::CreateKart
//
// param1: Player who owns kart.
//
// Description: Create the kart and assign it to player.
//-----------------------------------------------------
C_MKSClientKart *C_MKSClientKart::CreateKart( C_MKSPlayer *mksPlayer, const char *szModel )
{
	if( !mksPlayer )
		return NULL;

	C_MKSClientKart *mksKart = new C_MKSClientKart;//CREATE_ENTITY( C_LocalKart, "physics_kart" );

	if(!mksKart->InitializeAsClientEntity( szModel, RENDER_GROUP_OPAQUE_ENTITY ))
	{
		Msg( "Error Initializing C_MKSClientKart at local player\n" );
		return NULL;
	}

	mksPlayer->SetOwnerEntity( mksKart );
	//mksPlayer->SetParent( mksKart );
	mksKart->SetDriver( mksPlayer );

	mksKart->InitProfile();
	mksKart->InitializeKart();
	mksKart->SetGravity( 1.0 );

	Vector vOrigin = mksPlayer->GetAbsOrigin();
	QAngle qAngle = mksPlayer->GetAbsAngles();
	mksKart->SetAbsOrigin( vOrigin );	
	mksKart->SetAbsAngles( qAngle );

#ifdef DEBUG
	Warning( "Entity Created - \"mks_clientkart\" \n"  );
#endif
	
	return mksKart;
}

CON_COMMAND( create_gib, "Create a Gib Entity in front of you" )
{
	//Get player who called command.
	CMKSPlayer *mksPlayer = ToMKSPlayer( C_BasePlayer::GetLocalPlayer() );//UTIL_GetCommandClient() );
	if( !mksPlayer )
		return;
	
	Vector vOrigin = mksPlayer->GetAbsOrigin();
	Vector vF, vR, vU;
	mksPlayer->GetVectors( &vF, &vR, &vU );
	vOrigin += (vF * 100) + (vU * 50);
	AngularImpulse vAng = AngularImpulse(0,0,0);

	C_MKSClientKart *mksKart = C_MKSClientKart::CreateKart( mksPlayer, "models/characters/playable/kart/kart.mdl" );

	mksKart->VPhysicsGetObject()->SetPosition( vOrigin, QAngle(0,0,0), true );//SetAbsOrigin( vOrigin );
	mksKart->VPhysicsGetObject()->Wake();

	//C_Gib::CreateClientsideGib( "models/characters/playable/kart/kart.mdl", vOrigin, Vector(0,0,0), vAng );
	//CMKSCube::CreateCubeInFront( mksPlayer );
}
