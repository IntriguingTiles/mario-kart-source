#include "cbase.h"
#include "vcollide_parse.h"
#include "bone_setup.h"

#include "mks/Kart/c_mks_cube.h"
#include "mks/Player/c_mks_player.h"
#include "mks/Rules/mks_cvar_settings.h"


#define MKS_KART_MODEL "models/characters/playable/kart/kart.mdl"
//#define MKS_KART_MODEL "models/props/trees/treeguy.mdl"


IMPLEMENT_CLIENTCLASS_DT( C_MKSCube, DT_MKSCube, CMKSCube )
	RecvPropVector( RECVINFO( m_vCubeMins ) ),
	RecvPropVector( RECVINFO( m_vCubeMaxs ) ),
	RecvPropVector( RECVINFO( m_vCubeScale ) ),

	RecvPropFloat( RECVINFO( m_flDamping ) ),
	RecvPropFloat( RECVINFO( m_flRotDamping ) ),
	RecvPropFloat( RECVINFO( m_flDrag ) ),
	RecvPropFloat( RECVINFO( m_flRotDrag ) ),
	RecvPropFloat( RECVINFO( m_flMass ) ),
	RecvPropFloat( RECVINFO( m_flInertiaX ) ),
	RecvPropFloat( RECVINFO( m_flInertiaY ) ),
	RecvPropFloat( RECVINFO( m_flInertiaZ ) ),
END_RECV_TABLE()


//BEGIN_PREDICTION_DATA( C_MKSCube ) 
//END_PREDICTION_DATA()


LINK_ENTITY_TO_CLASS( mks_cube, C_MKSCube );


C_MKSCube::C_MKSCube()
{
	m_bCreated = false;

	m_vCubeMins = Vector(0,0,0);
	m_vCubeMaxs = Vector(0,0,0);
	m_vCubeScale = Vector(1,1,1);

	m_flDamping = 0;
	m_flRotDamping = 0;
	m_flDrag = 0;
	m_flRotDrag = 0;
	m_flMass = 0;
	m_flInertiaX = 0;
	m_flInertiaY = 0;
	m_flInertiaZ = 0;

}

C_MKSCube::~C_MKSCube()
{

}

/*
void C_MKSCube::CopyCube( C_MKSCube *pCopy )
{
	m_vCubeMins = pCopy->m_vCubeMins;
	m_vCubeMaxs = pCopy->m_vCubeMaxs;
	m_vCubeScale = pCopy->m_vCubeScale;

	m_flDamping = pCopy->m_flDamping;
	m_flRotDamping = pCopy->m_flRotDamping;
	m_flDrag = pCopy->m_flDrag;
	m_flRotDrag = pCopy->m_flRotDrag;
	m_flMass = pCopy->m_flMass;
	m_flInertiaX = pCopy->m_flInertiaX;
	m_flInertiaY = pCopy->m_flInertiaY;
	m_flInertiaZ = pCopy->m_flInertiaZ;
}
*/

//Scale the current model size of cube.
void C_MKSCube::ApplyBoneMatrixTransform( matrix3x4_t& transform )
{
	Vector vCubeScale = m_vCubeScale; //extract from NetworkVariable

	VectorScale( transform[0], vCubeScale[0], transform[0] );
	VectorScale( transform[1], vCubeScale[1], transform[1] );
	VectorScale( transform[2], vCubeScale[2], transform[2] );
}

int C_MKSCube::DrawModel( int flags )
{
	return BaseClass::DrawModel(flags);
}

//Initialize Physics on Cube.
bool C_MKSCube::InitializePhysics( void )
{	
	//if ( InitializeAsClientEntity( STRING(GetModelName()), RENDER_GROUP_OPAQUE_ENTITY ) == false )
	//{
	//	return false; 
	//}
	//Destroy any previous object
	VPhysicsDestroyObject();

	//Get the Min/Max Vectors of the current model.
	CreateMinMax();

	
	
	// create a normal physics object
	m_pPhysicsObject = PhysModelCreateOBB( this, m_vCubeMins, m_vCubeMaxs, GetAbsOrigin(), GetAbsAngles(), false );
	if ( !m_pPhysicsObject )
	{
		// failed to create a physics object
		Warning(" CMKSCube::CreateCube: PhysModelCreateOBB() failed for %s.\n", STRING(GetModelName()) );
		return false;
	}

	
	//VPhysicsSetObject( pCube );

	SetSolid( SOLID_VPHYSICS );
	SetSolidFlags( 0 );
	SetMoveType( MOVETYPE_VPHYSICS );

	m_pPhysicsObject->Wake();

	// We want touch calls when we hit the world
	unsigned int flags = m_pPhysicsObject->GetCallbackFlags();
	m_pPhysicsObject->SetCallbackFlags( flags | CALLBACK_GLOBAL_TOUCH_STATIC | CALLBACK_GLOBAL_TOUCH | CALLBACK_SHADOW_COLLISION | CALLBACK_GLOBAL_COLLIDE_STATIC);

	//SetCollisionGroup( COLLISION_GROUP_VEHICLE );

	//UpdatePartitionListEntry();
	//CollisionProp()->UpdatePartition();
	//UpdateVisibility();
	
	//SetBlocksLOS( false );

	return true;
}


//Get the Min/Max of model and set collision bounds.
void C_MKSCube::CreateMinMax()
{
	const model_t *mod = GetModel();
	if ( !mod )
	{
		Warning("CMKSCube::CreateCube: GetModel failed for entity %i.\n", GetModelIndex() );
		return;
	}

	Vector mins, maxs; 

	modelinfo->GetModelBounds( mod, mins, maxs );
	SetCollisionBounds( mins, maxs );

	m_vCubeMins = mins;
	m_vCubeMaxs = maxs;
}


void C_MKSCube::SetPhysicsParams( void )
{
	IPhysicsObject *pCube = VPhysicsGetObject();
	if(!pCube)
		return;

	float damping = m_flDamping;
	float rotdamping = m_flRotDamping;
	float drag = m_flDrag;
	float rotdrag = m_flRotDrag;

	pCube->SetDamping( &damping, &rotdamping );
	pCube->SetDragCoefficient( &drag, &rotdrag );
	pCube->SetInertia( Vector( m_flInertiaX, m_flInertiaY, m_flInertiaZ ) );
	pCube->SetMass( m_flMass );
}


void C_MKSCube::PostDataUpdate( DataUpdateType_t updateType )
{
	BaseClass::PostDataUpdate( updateType );
}

void C_MKSCube::OnDataChanged( DataUpdateType_t type )
{
	BaseClass::OnDataChanged( type );

	if ( type == DATA_UPDATE_CREATED )
	{
		m_bCreated = true;
	}
	else if( m_bCreated )
	{
		//InitializePhysics();
		//SetPhysicsParams();
	}

	UpdateVisibility();
}