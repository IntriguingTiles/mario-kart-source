#ifndef C_MKS_ENGINE_H
#define C_MKS_ENGINE_H

class C_MKSClientKart;

class C_MKSEngine {

public:

	C_MKSEngine();
	~C_MKSEngine();

	virtual void Update( void );

	static C_MKSEngine *CreateEngine( C_MKSClientKart *pOwner );

	virtual void Pedals( CUserCmd *pCmd );
	virtual void Gas( CUserCmd *pCmd );
	virtual void Brake( CUserCmd *pCmd );
	virtual void Neutral( CUserCmd *pCmd );

	virtual void Steering( CUserCmd *pCmd );
	virtual void Jump( CUserCmd *pCmd );

	virtual void Slide( CUserCmd *pCmd );
//Accessors
public:
	virtual C_MKSClientKart *GetKart( void );
	void SetKart( C_MKSClientKart *pKart ) { m_pKart = pKart; };

public:

	C_MKSClientKart *m_pKart;
	CUserCmd *m_pCmd; //Use this instead of passing it around.

	int m_iButtons;
	int m_iOldButtons;

	int m_iSlideDirection; // -1 = left, 1 = right 

	bool m_bCanReverse;
	
};


#endif //C_MKS_ENGINE_H