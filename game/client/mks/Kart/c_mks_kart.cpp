#include "cbase.h"
#include "vcollide_parse.h"
#include "bone_setup.h"

#include "mks/Kart/c_mks_engine.h"
#include "mks/Player/c_mks_player.h"
#include "mks/Kart/c_mks_kart.h"
#include "mks/Rules/mks_cvar_settings.h"
#include "mks/Kart/c_mks_clientkart.h"


#define MKS_KART_MODEL "models/characters/playable/kart/kart.mdl"


IMPLEMENT_CLIENTCLASS_DT( C_MKSKart, DT_MKSKart, CMKSKart )
	RecvPropFloat( RECVINFO( m_flKartVelocity ) ),
	RecvPropFloat( RECVINFO( m_flKartSpeed ) ),
	RecvPropFloat( RECVINFO( m_flMaxKartSpeed ) ),
	RecvPropFloat( RECVINFO( m_flThrottle ) ),
	RecvPropFloat( RECVINFO( m_flBrake ) ),
	RecvPropBool( RECVINFO( m_bSliding ) ),
	RecvPropFloat( RECVINFO( m_flSteering ) ),
	RecvPropFloat( RECVINFO( m_flFriction ) ),

	RecvPropDataTable( RECVINFO_DT( m_pProfile ), 0, &REFERENCE_RECV_TABLE( DT_MKSKartProfileInfo ) ),
END_RECV_TABLE()



//BEGIN_PREDICTION_DATA( C_MKSKart )  
//END_PREDICTION_DATA()


LINK_ENTITY_TO_CLASS( mks_kart, C_MKSKart );

C_MKSKart::C_MKSKart()
{
	m_flKartVelocity = 0.0f;
	m_flKartSpeed = 0.0f;
	m_flMaxKartSpeed = 0.0f;
	m_flThrottle = 0.0f;
	m_flBrake = 0.0f;
	m_flSteering = 0.0f;
	m_bSliding = false;
	m_flFriction = 0.0f; //Strong Friction

	m_pDriver = NULL;
	m_pEngine = NULL;

	m_pProfile = MKSPlayerProfile();
} 

C_MKSKart::~C_MKSKart()
{
	SetDriver( NULL );
	m_pEngine = NULL;
}

void C_MKSKart::Spawn( void )
{
	//BaseClass::Spawn();
}

void C_MKSKart::Teleport( const Vector *newPosition, const QAngle *newAngles, const Vector *newVelocity )
{
		
}

//-----------------------------------------------------
// CMKSKart::GetDriver
//
// returns: Current Driver
//
// Description: Gets the pointer to the current player driver.
//-----------------------------------------------------
C_MKSPlayer *C_MKSKart::GetDriver(void)
{
	return m_pDriver;
}

//-----------------------------------------------------
// CMKSKart::SetDriver
//
// param1: Player who will drive.
//
// Description: Saves the player pointer to the driver seat.
//-----------------------------------------------------
void C_MKSKart::SetDriver( C_MKSPlayer *mksPlayer )
{
	m_pDriver = mksPlayer;
}

void C_MKSKart::OnDataChanged( DataUpdateType_t type )
{
	BaseClass::OnDataChanged( type );

	if ( type == DATA_UPDATE_DATATABLE_CHANGED )
	{
		if( m_pProfile.id != -1 )
		{
			C_MKSPlayer *mksPlayer = ToMKSPlayer( C_BasePlayer::GetLocalPlayer() );
			if( !mksPlayer || mksPlayer->GetServerKart() ) return;

			SetModelName( m_pProfile.szModel );
			mksPlayer->SetServerKart( this );
			mksPlayer->InitClientKart();
		}
	}


}


/*
//-----------------------------------------------------
// C_MKSKart::CreateKart
//
// param1: Player who owns kart.
//
// Description: Create the kart and assign it to player.
//-----------------------------------------------------
C_MKSKart *C_MKSKart::CreateKart( C_MKSPlayer *mksPlayer )
{
	if( !mksPlayer )
		return NULL;

	//Get position and EyeVectors (forward, right, up)
	Vector vOrigin = mksPlayer->GetAbsOrigin();
	Vector vForward, vUp, vRight;
	mksPlayer->EyeVectors( &vForward, &vRight, &vUp );

	//Create a point from origin through forward vector fDistance units.
	Vector vCubeOrigin = vOrigin;
	QAngle angAngle = QAngle(0,0,0);

	C_MKSKart *mksKart = CREATE_ENTITY( C_MKSKart, "mks_kart" );

	if ( !mksKart )
		return NULL;

	mksPlayer->SetOwnerEntity( mksKart );
	mksPlayer->SetParent( mksKart );
	//mksKart->SetOwnerEntity( mksPlayer );
	mksKart->SetDriver( mksPlayer );
	mksKart->Spawn();


#ifdef DEBUG
	Warning( "Entity Created - \"mks_kart\" \n"  );
#endif


	angAngle = mksPlayer->GetAbsAngles();
	Vector vVelocity = Vector(0,0,0);

	
	//Set Model Position
	mksKart->SetAbsOrigin( vCubeOrigin );	

	IPhysicsObject *pKart = mksKart->VPhysicsGetObject();
	

	
	AngularImpulse angImpulse = AngularImpulse(0,0,0);

	pKart->SetPosition( vCubeOrigin, angAngle, true );
	pKart->SetVelocityInstantaneous( &vVelocity, &angImpulse );

	mksKart->Teleport( &vCubeOrigin, &angAngle, &vVelocity );
	return mksKart;
}
*/
