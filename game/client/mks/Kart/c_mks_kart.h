//--------------------------------------------------------------------------------------
//
//
//
//
//--------------------------------------------------------------------------------------

#ifndef MKS_KART_H
#define MKS_KART_H

#include "mks/Kart/c_mks_cube.h"
#include "mks/Kart/mks_kart_profile.h"

class C_MKSPlayer;
class C_MKSEngine;

class C_MKSKart : public C_MKSCube
{

public:

	DECLARE_CLASS( C_MKSKart, C_MKSCube );
	DECLARE_CLIENTCLASS();

public:

	C_MKSKart();
	~C_MKSKart();

	//DECLARE_PREDICTABLE();
	

	//static C_MKSKart *CreateClass( C_MKSPlayer *pOwner, const char *szModel );

	//bool CreateCube( void );
	//void SetCubeParams( void );
	virtual void Spawn( void );

	virtual void Teleport( const Vector *newPosition, const QAngle *newAngles, const Vector *newVelocity );

	virtual void OnDataChanged( DataUpdateType_t updateType );

//Accessors
public:
	virtual C_MKSPlayer *GetDriver(void);
	virtual void SetDriver( C_MKSPlayer *mksPlayer );

public:
	C_MKSPlayer *m_pDriver;
	C_MKSEngine *m_pEngine;

	MKSPlayerProfile m_pProfile;
	float m_flKartVelocity;

	float m_flKartSpeed;
	float m_flMaxKartSpeed;

	float m_flThrottle;
	float m_flBrake;

	float m_flSteering; 

	bool m_bSliding; //Kart Sliding parameter

	float m_flFriction; //Kart Friction against road
};

#endif //MKS_KART_H