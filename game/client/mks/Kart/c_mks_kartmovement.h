//--------------------------------------------------------------------------------------
//
//
//
//
//--------------------------------------------------------------------------------------

#ifndef C_MKS_KARTMOVEMENT_H
#define C_MKS_KARTMOVEMENT_H



class C_MKSClientKart;
class C_MKSPlayer;

class C_MKSKartMovement : public IMotionEvent
{
public:

	C_MKSKartMovement();
	~C_MKSKartMovement();

	//Create a new Movement class.
	static C_MKSKartMovement *CreateKartMovement( C_MKSClientKart *pKart );

	//Physics Simulation Frame
	virtual simresult_e	Simulate( IPhysicsMotionController *pController, IPhysicsObject *pObject, float deltaTime, Vector &linear, AngularImpulse &angular );

	virtual void CalculateVelocity( void );
	virtual void CalculateAngularVelocity( void );

	virtual void CalculateGravity( void );

	virtual C_MKSClientKart *GetKart() { return m_pKart; };
	virtual void SetKart( C_MKSClientKart *pKart ) { m_pKart = pKart; };

	virtual void DrawDebugLines();
	
	virtual C_MKSPlayer *GetDriver();
public:
	bool m_bActive;
	float m_flFrametime;

	C_MKSClientKart *m_pKart;

	Vector m_vFinalVelocity;

	IPhysicsMotionController *m_pController;
};

AngularImpulse MKSComputeRotSpeedToAlignAxes( const Vector &testAxis, const Vector &alignAxis, const AngularImpulse &currentSpeed, 
										  float damping, float scale, float maxSpeed );

#endif //C_MKS_KARTMOVEMENT_H