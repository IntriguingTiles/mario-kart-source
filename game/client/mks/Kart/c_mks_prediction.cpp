//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//
#include "cbase.h"
#include "prediction.h"
#include "c_baseplayer.h"
#include "igamemovement.h"

//#include "mks/kart/c_mks_prediction.h"
#include "mks/Player/c_mks_player.h"
#include "mks/Kart/c_mks_clientkart.h"

static CMoveData g_MoveData;
CMoveData *g_pMoveData = &g_MoveData;


class C_MKSPrediction : public CPrediction
{
DECLARE_CLASS( C_MKSPrediction, CPrediction );

public:
	virtual void	SetupMove( C_BasePlayer *player, CUserCmd *ucmd, IMoveHelper *pHelper, CMoveData *move );
	virtual void	FinishMove( C_BasePlayer *player, CUserCmd *ucmd, CMoveData *move );
};

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_MKSPrediction::SetupMove( C_BasePlayer *player, CUserCmd *ucmd, IMoveHelper *pHelper, 
	CMoveData *move )
{
	C_MKSPlayer *mksPlayer = ToMKSPlayer( player );
	if( !mksPlayer ) return;

	//C_MKSClientKart *mksKart = mksPlayer->GetClientKart();
	//if( !mksKart ) return;

	CUserCmd pCommand = *ucmd;
	mksPlayer->SetCurrentUserCmd( &pCommand );

	//Vector vOrigin = mksKart->GetAbsOrigin();
	//mksPlayer->SetAbsOrigin( vOrigin );

	// Call the default SetupMove code.
	BaseClass::SetupMove( player, ucmd, pHelper, move );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void C_MKSPrediction::FinishMove( C_BasePlayer *player, CUserCmd *ucmd, CMoveData *move )
{
	// Call the default FinishMove code.
	BaseClass::FinishMove( player, ucmd, move );
}


// Expose interface to engine
// Expose interface to engine
static C_MKSPrediction g_Prediction;

EXPOSE_SINGLE_INTERFACE_GLOBALVAR( C_MKSPrediction, IPrediction, VCLIENT_PREDICTION_INTERFACE_VERSION, g_Prediction );

CPrediction *prediction = &g_Prediction;

