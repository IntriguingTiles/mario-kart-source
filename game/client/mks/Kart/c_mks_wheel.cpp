#include "cbase.h"
#include "vcollide_parse.h"
#include "bone_setup.h"
#include "coordsize.h"

#include "mks/Player/c_mks_player.h"
#include "mks/Kart/c_mks_clientkart.h"
#include "mks/Kart/c_mks_wheel.h"
#include "mks/Rules/mks_cvar_settings.h"
#include "mks/Math/mks_math.h"




C_MKSWheel::C_MKSWheel()
{
	m_bIsOnGround = false;
	m_iWheelId = -1;

	m_iAttachment = -1;
	m_iSuspension = -1;

	m_flSuspensionDistance = 0.0f;
	m_flSuspensionPos = 0.0f;

	m_flRadius = 0.0f;
	m_flWidth = 0.0f;
	
	m_iSteer = 0;
	m_iSpeed = 0;

	m_bIsOnGround = false;
	m_bIsNearGround = false;


	m_pKart = NULL;

	m_pGroundEntity = NULL;
	SetGroundNormal( MKS_DEFAULT_GRAVITY );

	m_vLocalHigh = Vector(0,0,0);
	m_vLocalLow = Vector(0,0,0);
	m_vSpringDirection = Vector(0,0,0);

	m_vLocalOrigin = Vector(0,0,0);
	m_vWorldOrigin = Vector(0,0,0);

	m_vWheelVelocity = Vector(0,0,0);

	m_vDriveForce = Vector(0,0,0);
	m_vFrictionForce = Vector(0,0,0);
	m_vSuspensionForce = Vector(0,0,0);
}


C_MKSWheel::~C_MKSWheel()
{

}


C_MKSWheel *C_MKSWheel::CreateWheel( C_MKSClientKart *pOwner )
{
	C_MKSWheel *pWheel = new C_MKSWheel();
	
	if( pWheel == NULL )
		return NULL;

	pWheel->SetKart( pOwner );
	return pWheel;
}

void C_MKSWheel::Init( MKSPlayerProfile *pProfile )
{
	int id = m_iWheelId;
	//Add wheel to list
	if( m_iWheelId == -1 )
	{
		id = GetKart()->AddWheel( this );
		m_iWheelId = id;
	}

	if( id == -1 )
		return;

	m_iSteer = pProfile->iWheelSteer[id];
	m_iSpeed = pProfile->iWheelSpeed[id];
	m_flWidth = pProfile->flWheelWidth[id];
	m_flRadius = pProfile->flWheelRadius[id];
	m_iAttachment = pProfile->iWheelAttachment[id];
	m_iSuspension = pProfile->iWheelSuspension[id];

	C_MKSClientKart *mksKart = GetKart();
	QAngle dummy;

	//MKS-OneEyed FIXME: This needs to be from 0.0 (high) to 1.0 (low) 

	//High Point
	mksKart->SetPoseParameter( m_iSuspension, -1.0f );
	mksKart->InvalidateBoneCache();
	mksKart->GetAttachmentLocal( m_iAttachment, m_vLocalHigh, dummy );

	//Low Point
	mksKart->SetPoseParameter( m_iSuspension, 1.0f );
	mksKart->InvalidateBoneCache();
	mksKart->GetAttachmentLocal( m_iAttachment, m_vLocalLow, dummy );

	mksKart->SetPoseParameter( m_iSuspension, 0.0f );
	mksKart->InvalidateBoneCache();
	mksKart->GetAttachmentLocal( m_iAttachment, m_vLocalOrigin, dummy );

	//m_vLocalHigh.z -= m_flRadius + DIST_EPSILON;
	//m_vLocalLow.z -= m_flRadius + DIST_EPSILON;

	//m_vLocalOrigin = m_vLocalHigh;

	m_vSpringDirection = m_vLocalLow - m_vLocalHigh;
	m_flSuspensionDistance = VectorLength( m_vSpringDirection );
	m_flSuspensionPos = 0.0f; //Lowest Possible
}



void C_MKSWheel::Update( void )
{
	UpdateValues();

	FindGround();

	Suspension();

	Steering();

	Friction();

	DriveForce();
}


void C_MKSWheel::UpdateValues( void )
{
	C_MKSClientKart *mksKart = GetKart();

	//Update Local Origin
	QAngle dummy;
	//mksKart->GetAttachmentLocal( m_iAttachment, m_vLocalOrigin, dummy );
	
	//m_flSuspensionDistance = 1.0;
	//m_vLocalOrigin = m_vLocalHigh;//m_vLocalHigh + ((m_vLocalLow - m_vLocalHigh) * 0.5f);//+ (m_vSpringDirection * m_flSuspensionDistance);

	////Update World Origin of Tire
	m_vWorldOrigin = mksKart->GetWorldOrigin();
	m_vWorldOrigin += m_vLocalOrigin.x * mksKart->GetForward();
	m_vWorldOrigin += m_vLocalOrigin.y * mksKart->GetRight();
	m_vWorldOrigin += m_vLocalOrigin.z * mksKart->GetUp();
}


void C_MKSWheel::Steering( void )
{
}


void C_MKSWheel::FindGround( void )
{
	C_MKSClientKart *mksKart = GetKart();

	Vector vStart = m_vWorldOrigin + (mksKart->GetUp() * GetRadius());
	Vector vEnd = vStart + (mksKart->GetUp() * -MAX_TRACE_LENGTH);//Vector( 0, 0, MAX_TRACE_LENGTH );

	trace_t tr;
	UTIL_TraceLine( vStart, vEnd, MASK_PLAYERSOLID_BRUSHONLY, mksKart, COLLISION_GROUP_MKS_EXTRA1, &tr );

	SetGroundDistance( (tr.fraction * MAX_TRACE_LENGTH) - GetRadius() );

	if( IsNearGround() )
	{
		SetGroundNormal( tr.plane.normal );
		SetGroundEntity( tr.m_pEnt );
		//SetIsNearGround( true );
	}
	else
	{
		//SetGroundNormal( MKS_DEFAULT_GRAVITY );
		SetGroundEntity( NULL );
		//SetIsNearGround( false );
	}
}


void C_MKSWheel::DriveForce( void )
{
	if( !IsNearGround() || m_iSteer <= 0)
		return;

	C_MKSClientKart *mksKart = GetKart();
	IPhysicsObject *pKart = mksKart->VPhysicsGetObject();

	Vector vForward = mksKart->GetForward();
	Vector vRight = mksKart->GetRight();
	Vector vUp = mksKart->GetUp();
	Vector vGravityDir = mksKart->GetGravityDirection();
	
	float flSteer = mksKart->GetSteering();
	float flSpeed = mksKart->GetKartSpeed();

	//QAngle qLocalAngle = QAngle( 0, flSteer, 0 );
	//Vector vLocalForward;

	//AngleVectors( qLocalAngle, &vLocalForward );

	//mksKart->EntityToWorldSpace( vLocalForward, &vForward );

	//Vector vOrigin = mksKart->GetWorldOrigin();
	//vForward = vForward - vOrigin;
	//vForward = mksKart->GetForward();

	m_vDriveForce = ((vForward * flSpeed)) / mksKart->WheelCount();
	//ClipVelocity( m_vDriveForce, vGravityDir, m_vDriveForce, 1.0 );
	pKart->ApplyForceCenter( m_vDriveForce );

	m_vFrictionForce = ((vRight * (-flSteer)) / mksKart->WheelCount());
	pKart->ApplyForceOffset( m_vFrictionForce, m_vWorldOrigin );
	//m_vDriveForce = vForward * flSpeed;
	//pKart->ApplyForceCenter( m_vDriveForce );
}


void C_MKSWheel::Suspension( void )
{
	C_MKSClientKart *mksKart = GetKart();
	IPhysicsObject *pKart = mksKart->VPhysicsGetObject();

	Vector vStart = m_vWorldOrigin;
	Vector vEnd = vStart - Vector( 0, 0, m_flRadius + (DIST_EPSILON*2.0f) );

	trace_t tr;
	UTIL_TraceLine( vStart, vEnd, MASK_SOLID, mksKart, COLLISION_GROUP_NONE, &tr );

	//float flDistance = m_flRadius + (DIST_EPSILON*2.0f);

	//if(  tr.fraction < 1.0 )
	if( IsOnGround() )
	{
		Vector vGravityDir = mksKart->GetGravityDirection();
		Vector vVelocity;

		pKart->GetVelocityAtPoint( m_vWorldOrigin, &vVelocity );

		float flDistance = VectorLength( (vEnd - vStart) ) * (1.0f - tr.fraction);
		float flSpringConstant = mks_spring_stiff.GetFloat();
		float flDamping = mks_spring_damping.GetFloat();

		Vector pointVel = vVelocity;
		pointVel = DotProduct(pointVel, vGravityDir) * vGravityDir;
		
		m_vSuspensionForce = (( (-flSpringConstant * (vGravityDir * flDistance)) + (-flDamping * pointVel) ) * gpGlobals->frametime) / mksKart->WheelCount();
		
		pKart->ApplyForceOffset( m_vSuspensionForce, m_vWorldOrigin );

		SetGroundEntity( tr.m_pEnt );
		//SetIsOnGround( true );
	}
	else
	{
		SetGroundEntity( NULL );
		//SetIsOnGround( false );
	}
}



void C_MKSWheel::Friction( void )
{
	C_MKSClientKart *mksKart = GetKart();
	if( !mksKart )
		return;

	IPhysicsObject *pKart = mksKart->VPhysicsGetObject();
	if( !pKart )
		return;

	if( IsOnGround() )
	{
		Vector vVelocity;
		pKart->GetVelocityAtPoint( m_vWorldOrigin, &vVelocity );

		Vector vForward = mksKart->GetForward();
		Vector vRight = mksKart->GetRight();

		float flFrictionMult = mks_friction_multiplier.GetFloat();
		float flFrictionKart = mksKart->GetWheelFriction();
		float flSpeed = mksKart->GetKartSpeed();

		 //calculate speed of tire patch at ground
        Vector patchSpeed = -vForward * flSpeed * GetRadius();

        //get velocity difference between ground and patch
        Vector velDifference = vVelocity + patchSpeed;

        //project ground speed onto side axis
        Vector sideVel = DotProduct(velDifference, vRight) * vRight;
		float forwardMag = DotProduct(velDifference, vForward);
        Vector forwardVel = vForward * forwardMag;

        //calculate super fake friction forces
        //calculate response force
        Vector responseForce = -sideVel * flFrictionKart * flFrictionMult;
        responseForce -= forwardVel;

		pKart->ApplyForceCenter( responseForce );
	}
}