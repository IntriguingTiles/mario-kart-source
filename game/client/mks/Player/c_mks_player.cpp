//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"

#include "c_user_message_register.h"
#include "c_basetempentity.h"
#include "prediction.h"
#include "view.h"
#include "iviewrender.h"
#include "ivieweffects.h"
#include "view_scene.h"
#include "fx.h"
#include "collisionutils.h"
#include "c_sdk_team.h"
#include "obstacle_pushaway.h"
#include "bone_setup.h"
#include "cl_animevent.h"

#include "mks/Camera/mks_camera.h"
#include "mks/Player/c_mks_player.h"
#include "mks/Kart/c_mks_clientkart.h"
#include "mks/Kart/c_mks_kart.h"

//it might make sense in the future to ues a array or somthing for player models
#define	ENTITY_MODEL	"models/models/characters/playable/mario/mario.mdl"

#if defined( CMKSPlayer )
	#undef CMKSPlayer
#endif

BEGIN_RECV_TABLE_NOBASE( C_MKSPlayer, DT_MKSLocalPlayerExclusive )
END_RECV_TABLE()

BEGIN_RECV_TABLE_NOBASE( C_MKSPlayer, DT_MKSNonLocalPlayerExclusive )
END_RECV_TABLE()

// main table
IMPLEMENT_CLIENTCLASS_DT( C_MKSPlayer, DT_MKSPlayer, CMKSPlayer )
	RecvPropDataTable( "mkslocaldata", 0, 0, &REFERENCE_RECV_TABLE(DT_MKSLocalPlayerExclusive) ),
	RecvPropDataTable( "mksnonlocaldata", 0, 0, &REFERENCE_RECV_TABLE(DT_MKSNonLocalPlayerExclusive) ),
	RecvPropEHandle(RECVINFO(m_hCurrentItem)),
	RecvPropBool(RECVINFO(m_bHasItem)),
END_RECV_TABLE()

// ------------------------------------------------------------------------------------------ //
// Prediction tables.
// ------------------------------------------------------------------------------------------ //
BEGIN_PREDICTION_DATA( C_MKSPlayer )
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( mks_player, C_MKSPlayer );



C_MKSPlayer::C_MKSPlayer()
{
	MKSCamera *pCamera = new MKSCamera();
	SetCamera( pCamera );

	SetClientKart( NULL );
	SetServerKart( NULL );
	SetKartName( "Default" );


	m_bHasItem = false;
	m_hCurrentItem = 0;

}


C_MKSPlayer::~C_MKSPlayer()
{
	m_pCamera->Release();

	m_hCurrentItem = 0;

	CUserCmd pCmd = CUserCmd();
	SetCurrentUserCmd( &pCmd );

	C_MKSClientKart *clientKart = GetClientKart();
	if( clientKart ) {
		clientKart->Release();
	}

	C_MKSKart *serverKart = GetServerKart();
	if( serverKart ) {
		serverKart->Release();
	}
	
	SetOwnerEntity( NULL );
	SetParent( NULL );
}


void C_MKSPlayer::LocalPlayerRespawn( void )
{
	BaseClass::LocalPlayerRespawn();

	
}


void C_MKSPlayer::CalcView( Vector &eyeOrigin, QAngle &eyeAngles, float &zNear, float &zFar, float &fov )
{
	BaseClass::CalcView( eyeOrigin, eyeAngles, zNear, zFar, fov );
	//GetCamera()->UpdateCamera( false );
	//GetCamera()->GetCameraInfo( eyeOrigin, eyeAngles );

	//engine->SetViewAngles( eyeAngles );
}



void C_MKSPlayer::ClientThink( void )
{
	//GetCamera()->UpdateCamera( false );
	////Vector vOrigin;
	//QAngle qViewAngle;
	//GetCamera()->GetCameraInfo( vOrigin, qViewAngle );

	//engine->SetViewAngles( qViewAngle );

}

void C_MKSPlayer::InitClientKart() 
{
	if( !GetServerKart() ) return;
	if( GetClientKart() ) return;
	SetCollisionGroup( COLLISION_GROUP_MKS_PLAYER );

	//if( State_Get() == STATE_ACTIVE )
	{
		C_MKSClientKart *mksKart = C_MKSClientKart::CreateKart( (C_MKSPlayer *)this, GetServerKart()->GetModelName() );
		if( !mksKart ) return;
		SetClientKart( mksKart );

		Vector origin = GetAbsOrigin();
		QAngle angle = GetAbsAngles();

		mksKart->SetAbsOrigin( origin );
		mksKart->SetLocalOrigin( origin );
	}
	//SetModel(ENTITY_MODEL);
}

void C_MKSPlayer::OnDataChanged( DataUpdateType_t type )
{
	BaseClass::OnDataChanged( type );

	if ( type == DATA_UPDATE_CREATED )
	{
		SetNextClientThink( CLIENT_THINK_ALWAYS );

		//MKS-OneEyed - Create Kart Camera and teleport it into position.
		//GetCamera()->SetTarget(this);
		//GetCamera()->UpdateCamera( true );

	}
	else
	{
		
	}


	C_BaseEntity *pKart = GetOwnerEntity();
	if( pKart != NULL && GetCamera()->GetTarget() != GetOwnerEntity() )
	{
		GetCamera()->SetTarget( GetOwnerEntity() );
	}

	UpdateVisibility();
}
