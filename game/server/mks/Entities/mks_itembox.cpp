//======================================================================\\
// MKS-OneEyed: Creates a itembox on the mapper defined locations.
// TODO: Random weapons spawning.
//======================================================================//
#include "cbase.h"


#include "mks/Player/mks_player.h"
#include "mks/Kart/mks_kart.h"
#include "mks/Items/mks_item.h"


class CMKSItemBox : public CBaseAnimating
{
public:
	DECLARE_CLASS( CMKSItemBox, CBaseAnimating );
	DECLARE_SERVERCLASS();
	DECLARE_DATADESC();
	CMKSItemBox();

	void Spawn( void );
	void Precache( void );

	void Think( void );

	void	PropSetSequence( int nSequence );

	// Input function

	short				m_nPendingSequence;

	void Touch(CBaseEntity *pOther);
	//void SearchPlayerInArea( Vector &origin );


private:
	void PickNewItem();

	float	m_flNextReadyTime;

	CNetworkVar( bool, m_bActive );

	float	m_flNextChangeTime;
	MKSItemID m_iItemID;
protected:
	void FinishSetSequence( int nSequence );
	void PropSetAnim( const char *szAnim );

	
};

LINK_ENTITY_TO_CLASS( mks_itembox, CMKSItemBox );


IMPLEMENT_SERVERCLASS_ST( CMKSItemBox, DT_MKSItemBox )
	SendPropBool(SENDINFO(m_bActive)),
END_SEND_TABLE()



// Start of our data description for the class
BEGIN_DATADESC( CMKSItemBox )
	
	// Save/restore our active state
	DEFINE_FIELD( m_bActive, FIELD_BOOLEAN ),
	DEFINE_FIELD( m_flNextChangeTime, FIELD_TIME ),

	// Declare our think function
	DEFINE_THINKFUNC( Think ),
	DEFINE_ENTITYFUNC( Touch ),
	
	
END_DATADESC()




// Name of our entity's model
#define	ENTITY_MODEL	"models/misc/itembox/itembox.mdl"
//#define	ENTITY_MODEL	"models/player/red_player.mdl"

CMKSItemBox::CMKSItemBox()
{
	m_flNextReadyTime = 0;
	m_nPendingSequence = -1;
	if ( g_pGameRules->IsMultiplayer() )
	{
		UseClientSideAnimation();
	}
}

//-----------------------------------------------------------------------------
// Purpose: Precache assets used by the entity
//-----------------------------------------------------------------------------
void CMKSItemBox::Precache( void )
{
	PrecacheModel( ENTITY_MODEL );
}

//-----------------------------------------------------------------------------
// Purpose: Sets up the entity's initial state
//-----------------------------------------------------------------------------
void CMKSItemBox::Spawn( void )
{
	Precache();

	SetModel( ENTITY_MODEL );
	/*
	SetRenderMode( kRenderTransAlpha );
	m_nRenderFX = kRenderFxNone;
	SetRenderColor( 0, 0, 0, 0 );
	*/
	//m_nRenderFX = kRenderFxNone;
	//SetSolidFlags( FSOLID_TRIGGER | FSOLID_NOT_SOLID );

	// Start thinking
	SetSolid( SOLID_BBOX );
	SetSolidFlags( FSOLID_TRIGGER | FSOLID_NOT_SOLID );
	SetCollisionGroup( COLLISION_GROUP_MKS_ITEMBOX );
	SetSize( Vector(-12,-12,5), Vector(12,12,35));
	SetThink( &CMKSItemBox::Think );
	SetTouch( &CMKSItemBox::Touch );
	//SetThink( Think );
	SetNextThink( gpGlobals->curtime + 0.05f );
	//PropSetAnim( "Idle" );
	PickNewItem();
	m_bActive = true;
}

void CMKSItemBox::PickNewItem()
{
	m_iItemID = static_cast<MKSItemID>(random->RandomInt(ITEM_NONE+1, ITEM_MAX-1));
}

//------------------------------------------------------------------------------
// Purpose: Sets an animation by sequence name or activity name.
//------------------------------------------------------------------------------
void CMKSItemBox::PropSetAnim( const char *szAnim )
{
	if ( !szAnim )
		return;

	int nSequence = LookupSequence( szAnim );

	// Set to the desired anim, or default anim if the desired is not present
	if ( nSequence > ACTIVITY_NOT_AVAILABLE )
	{
		PropSetSequence( nSequence );

		// Fire output
		//m_pOutputAnimBegun.FireOutput( NULL,this );
	}
	else
	{
		// Not available try to get default anim
		Msg( "Dynamic prop %s: no sequence named:%s\n", GetDebugName(), szAnim );
		SetSequence( 0 );
	}
}

//------------------------------------------------------------------------------
// Purpose: Search for players and gives them weapons.
//------------------------------------------------------------------------------
void CMKSItemBox::Touch(CBaseEntity *pOther)//SearchPlayerInArea( Vector &origin )
{
	if(!m_bActive)
		return;
	
	if((pOther->IsPlayer() || Q_strcmp(pOther->GetClassname(), "mks_kart") == 0) && m_flNextReadyTime == 0.0)
	{
		CMKSPlayer *pPlayer = 0;
		if (!pOther->IsPlayer()){
			CMKSKart *kart = dynamic_cast<CMKSKart *>(pOther);
			pPlayer = kart->m_pDriver;
		}else{
			pPlayer = ToMKSPlayer( pOther );
		}
		if(pPlayer)
		{
			
			if (!pPlayer->HasItem()){
				//give the player their item
				pPlayer->SetCurrentItem(CreateItem(m_iItemID, pPlayer));
			}
			m_iItemID = ITEM_NONE;
			m_bActive = false;
			AddEffects( EF_NOSHADOW | EF_NORECEIVESHADOW );
			m_flNextReadyTime = gpGlobals->curtime + 5.0;
		}
	}
	
}

//-----------------------------------------------------------------------------
// Purpose: Helper in case we have to async load the sequence
// Input  : nSequence - 
//-----------------------------------------------------------------------------
void CMKSItemBox::FinishSetSequence( int nSequence )
{
	float rand = random->RandomFloat( 0.0f, 1.0f );
	SetCycle( rand );
	//SetCycle( 0 );
	m_flAnimTime = gpGlobals->curtime;
	ResetSequence( nSequence );
	ResetClientsideFrame();
	RemoveFlag( FL_STATICPROP );
}

//-----------------------------------------------------------------------------
// Purpose: Sets the sequence and starts thinking.
// Input  : nSequence - 
//-----------------------------------------------------------------------------
void CMKSItemBox::PropSetSequence( int nSequence )
{
	float flInterval = 0.1f;
	if ( PrefetchSequence( nSequence ) )
	{
		FinishSetSequence( nSequence );
	}
	else
	{
		m_nPendingSequence = nSequence;
		// Check every tick until the data arrives
		flInterval = 0.0f;
	}

	//SetThink( &CMKSItemBox::Think );
	//SetNextThink( gpGlobals->curtime + flInterval );
}

//-----------------------------------------------------------------------------
// Purpose: Search for players, handle animation
//-----------------------------------------------------------------------------
void CMKSItemBox::Think( void )
{
	if( !m_bActive )
	{
		if(m_flNextReadyTime <= gpGlobals->curtime )
		{
			m_flNextReadyTime = 0.0;
			m_bActive = true;
			PickNewItem();
			RemoveEffects( EF_NOSHADOW | EF_NORECEIVESHADOW );
			//SetRenderColorA( 255 );

			//SetRenderMode( kRenderNormal );
			//m_nRenderFX = kRenderFxNone;
			//SetRenderColorA( 255 );
			//RemoveEFlags( EF_NODRAW );
			//SetSolid( SOLID_BBOX );
		}
		/*else
		{
			color32 color;
			color = GetRenderColor();
			if(color.a > 0)
			{
				SetRenderColorA( color.a - (byte)1 );
			}
		}*/
	}

/*
	if ( m_nPendingSequence != -1 )
	{
		if ( PrefetchSequence( m_nPendingSequence ) )
		{
			FinishSetSequence( m_nPendingSequence );
			m_nPendingSequence = -1;
		}
		else
		{
			SetNextThink( gpGlobals->curtime + 0.05f );
			return;
		}
	}

	if ( GetCycle() >= 0.97f && !SequenceLoops() )
	{
		PropSetAnim( "Idle" );
	}
	//else
	//{
	//	SetNextThink( gpGlobals->curtime );
	//}

	StudioFrameAdvance();
	DispatchAnimEvents(this);

	//Search players hitting the model
	//Vector vecAbsorigin = GetAbsOrigin();
	//SearchPlayerInArea( vecAbsorigin );
*/
	// Think every 20Hz
	SetNextThink( gpGlobals->curtime + 0.05f );
}

