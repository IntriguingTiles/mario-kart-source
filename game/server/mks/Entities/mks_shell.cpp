#include "cbase.h"
#include "mks_shell.h"

#define	ENTITY_MODEL	"models/weapons/gshell_model.mdl"

LINK_ENTITY_TO_CLASS( mks_shell, Shell );
PRECACHE_REGISTER(mks_shell);

BEGIN_DATADESC( Shell )
	DEFINE_ENTITYFUNC( Touch ),
END_DATADESC()


Shell *Shell::CreateShell(MKSShellType type, const Vector &vecOrigin, const QAngle &vecAngles, CMKSPlayer *pOwner)
{
	Shell *shell = dynamic_cast<Shell *>(CreateEntityByName("mks_shell"));
	shell->SetAbsAngles(vecAngles);
	shell->SetAbsOrigin(vecOrigin);
	shell->SetType(type);
	shell->SetModel(ENTITY_MODEL);
	return shell;
}

Shell::Shell()
{
	m_eShellType = SHELL_NONE;
	


}

void Shell::Spawn()
{
	BaseClass::Spawn();
}

void Shell::Precache()
{
	PrecacheModel( ENTITY_MODEL );
}

void Shell::StartSpin()
{
	//does not keep spinning...
	SetSequence(LookupSequence("spin"));
	SetPlaybackRate( 1.0f );
	UseClientSideAnimation();

}

void Shell::Fire(const QAngle &vecAngles, int forwardVelocity)
{
	//stop following the kart
	FollowEntity(0);

	Vector forward;
	AngleVectors(vecAngles, &forward);
	SetAbsAngles(vecAngles);

	SetAbsVelocity(forward*forwardVelocity);
	SetMoveType( MOVETYPE_FLYGRAVITY );
	SetSolid( SOLID_NONE );

}

void Shell::Touch(CBaseEntity *pOther)
{
}
