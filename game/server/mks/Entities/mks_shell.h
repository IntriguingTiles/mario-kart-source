#pragma once

typedef enum
{
	SHELL_NONE,
	SHELL_GREEN,
	SHELL_RED,
} MKSShellType;

class CMKSPlayer;

class Shell : public CBaseAnimating
{
public:
	DECLARE_CLASS( Shell, CBaseAnimating );
	DECLARE_DATADESC();
	Shell();
	static Shell *CreateShell(MKSShellType type, const Vector &vecOrigin, const QAngle &vecAngles, CMKSPlayer *pOwner);
	void SetType(MKSShellType type) {m_eShellType = type;}
	void Precache();
	void StartSpin();
	void Fire(const QAngle &vecAngles, int forwardVelocity);
	void Spawn();
	void Touch(CBaseEntity *pOther);
private:
	MKSShellType m_eShellType;
	CMKSPlayer *m_pOwner;
};
