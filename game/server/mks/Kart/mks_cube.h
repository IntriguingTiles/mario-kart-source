//--------------------------------------------------------------------------------------
//
//
//
//
//--------------------------------------------------------------------------------------

#ifndef MKS_CUBE_H
#define MKS_CUBE_H


class CMKSPlayer;
class CMKSKartMovement;


class CMKSCube : public CBaseAnimating
{
	DECLARE_CLASS( CMKSCube, CBaseAnimating );
public:

	CMKSCube();
	~CMKSCube();

	DECLARE_SERVERCLASS();


	void Precache( void );
	void Spawn( void );

	static CMKSCube *CreateCube( CMKSPlayer *pOwner, const char* szModel );

	virtual bool InitializePhysics( void );
	virtual void SetPhysicsParams( void );

	virtual void CreateMinMax( void );

	static CMKSCube *CreateCubeInFront( CMKSPlayer *mksPlayer );

	//virtual bool ShouldPredict( void ) { return true; };

	Vector GetSpawnOrigin() { return m_vSpawnOrigin; };
	void SetSpawnOrigin( Vector origin ) { m_vSpawnOrigin = origin; };

//Variables
public:

	Vector m_vSpawnOrigin;

	char *m_szCubeModel;
	CNetworkVar( Vector, m_vCubeMins );
	CNetworkVar( Vector, m_vCubeMaxs );
	CNetworkVar( Vector, m_vCubeScale );


	CNetworkVar( float, m_flDamping );
	CNetworkVar( float, m_flRotDamping );
	CNetworkVar( float, m_flDrag );
	CNetworkVar( float, m_flMass );
	CNetworkVar( float, m_flRotDrag );
	CNetworkVar( float, m_flInertiaX );
	CNetworkVar( float, m_flInertiaY );
	CNetworkVar( float, m_flInertiaZ );

};

#endif //MKS_CUBE_H