#ifndef MKS_ENGINE_H
#define MKS_ENGINE_H

class CMKSKart;

class CMKSEngine {

public:

	CMKSEngine();
	~CMKSEngine();


	virtual void Update( void );

	static CMKSEngine *CreateEngine( CMKSKart *pOwner );

	virtual void Pedals( CUserCmd *pCmd );
	virtual void Gas( CUserCmd *pCmd );
	virtual void Brake( CUserCmd *pCmd );
	virtual void Neutral( CUserCmd *pCmd );

	virtual void Steering( CUserCmd *pCmd );
	virtual void Jump( CUserCmd *pCmd );

	virtual void Slide( CUserCmd *pCmd );
//Accessors
public:
	virtual CMKSKart *GetKart( void );
	void SetKart( CMKSKart *pKart ) { m_pKart = pKart; };

public:

	CMKSKart *m_pKart;
	CUserCmd *m_pCmd; //Use this instead of passing it around.

	int m_iButtons;
	int m_iOldButtons;

	int m_iSlideDirection; // -1 = left, 1 = right 

	bool m_bCanReverse;
};


#endif //MKS_ENGINE_H