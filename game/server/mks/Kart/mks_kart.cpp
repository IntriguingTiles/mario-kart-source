#include "cbase.h"
#include "vcollide_parse.h"
#include "bone_setup.h"

#include "mks/Player/mks_player.h"
#include "mks/Kart/mks_kart.h"
#include "mks/Kart/mks_kartmovement.h"
#include "mks/Kart/mks_wheel.h"
#include "mks/Kart/mks_engine.h"
#include "mks/Rules/mks_cvar_settings.h"



//#define MKS_KART_MODEL "models/characters/playable/kart/kart.mdl"

PRECACHE_REGISTER( mks_kart );


IMPLEMENT_SERVERCLASS_ST(CMKSKart, DT_MKSKart)

	SendPropFloat( SENDINFO( m_flKartVelocity ), 16, SPROP_NOSCALE, -2500.0f, 2500.0f ),
	SendPropFloat( SENDINFO( m_flKartSpeed ), 16, SPROP_NOSCALE, -2500.0f, 2500.0f ),
	SendPropFloat( SENDINFO( m_flMaxKartSpeed ), 16, SPROP_NOSCALE, -2500.0f, 2500.0f ),
	SendPropFloat( SENDINFO( m_flThrottle ), 10, SPROP_NOSCALE, -1.0f, 1.0f ),
	SendPropFloat( SENDINFO( m_flBrake ), 10, SPROP_NOSCALE, -1.0f, 1.0f ),
	SendPropFloat( SENDINFO( m_flSteering ), 10, SPROP_NOSCALE, -1.0f, 1.0f ),
	SendPropBool( SENDINFO( m_bSliding ) ),
	SendPropFloat( SENDINFO( m_flFriction ), 10, SPROP_NOSCALE ),

	//Send the profile
	SendPropDataTable( SENDINFO_DT( m_pProfile ), &REFERENCE_SEND_TABLE( DT_MKSKartProfileInfo ) ),
END_SEND_TABLE()

LINK_ENTITY_TO_CLASS( mks_kart, CMKSKart );

CMKSKart::CMKSKart()
{
	m_flKartVelocity = 0.0f;
	m_flKartSpeed = 0.0f;
	m_flMaxKartSpeed = 0.0f;
	m_flThrottle = 0.0f;
	m_flBrake = 0.0f;
	m_flSteering = 0.0f;
	m_bSliding = false;
	m_flFriction = 0.0f; //Strong Friction

	m_pDriver = NULL;
	m_pMovement = NULL;
	m_pEngine = NULL;
	SetGravityDirection( Vector(0,0,-1) );

	ClearAddVelocity();

	m_iWheelGroundCount = 0;
	m_iWheelCount = 0;

	m_vAddVelocity = Vector(0,0,0);
	m_angVelocity = AngularImpulse(0,0,0);

	m_vWorldOrigin = Vector(0,0,0);
	m_vForward = Vector(0,0,0);
	m_vRight = Vector(0,0,0);
	m_vUp = Vector(0,0,0);
}

CMKSKart::~CMKSKart()
{

}

//-----------------------------------------------------
// CMKSKart::PhysicsSolidMaskForEntity
//
// returns: Solid Mask
//
// Description: The Solid Mask of entity.
//-----------------------------------------------------
unsigned int CMKSKart::PhysicsSolidMaskForEntity( void ) const
{
	return MASK_PLAYERSOLID;
}

//-----------------------------------------------------
// CMKSKart::GetDriver
//
// returns: Current Driver
//
// Description: Gets the pointer to the current player driver.
//-----------------------------------------------------
CMKSPlayer *CMKSKart::GetDriver(void)
{
	return m_pDriver;
}

//-----------------------------------------------------
// CMKSKart::SetDriver
//
// param1: Player who will drive.
//
// Description: Saves the player pointer to the driver seat.
//-----------------------------------------------------
void CMKSKart::SetDriver( CMKSPlayer *mksPlayer )
{
	m_pDriver = mksPlayer;
}


//-----------------------------------------------------
// CMKSKart::SetProfile
//
// param1: Kart Profile Name (found in the kart script files)
//
// Description: Copy the profile data into our MKSPlayerProfile struct.
//-----------------------------------------------------
void CMKSKart::SetProfile( const char *szKartName )
{
	m_pProfile.Copy( g_KartProfiles->GetProfile( szKartName ) );
}


//-----------------------------------------------------
//CMKSKart::Precache
//
//Description: Precache Kart Model from Profile
//-----------------------------------------------------
void CMKSKart::Precache( void )
{
	BaseClass::Precache();
}


//-----------------------------------------------------
// CMKSKart::GetProfileIndexes
//
// param1: Profile from kart scripts
//
// Description: Update the internal values
//-----------------------------------------------------
void CMKSKart::GetProfileIndexes( MKSPlayerProfile *pProfile )
{
	int b;
	int iWheelCount = pProfile->iWheelCount;
	for( b=0; b<iWheelCount; b++ )
	{
		const char *szAttachment = pProfile->szWheelAttachment[b];
		int iAttach = LookupAttachment( szAttachment );
		pProfile->iWheelAttachment.Set( b, iAttach );

		const char *szSuspension = pProfile->szWheelSuspension[b];
		int iPose = LookupPoseParameter( szSuspension );
		pProfile->iWheelSuspension.Set( b, iPose );
	}
}


//-----------------------------------------------------
// CMKSKart::Spawn
//
// Description: Called when player spawns, setup kart.
//-----------------------------------------------------
void CMKSKart::Spawn( void )
{
	InitProfile();

	BaseClass::Spawn();

	SetGravity( 1.0f );
	
	InitializeKart();
}


//-----------------------------------------------------
// CMKSKart::Run
//
// Description: Process all moving parts
//-----------------------------------------------------
void CMKSKart::Update( void )
{
	UpdateValues();

	//Process movement engine
	CMKSEngine *mksEngine = GetEngine();
	mksEngine->Update();

	//Process all tires.
	CMKSWheel *mksWheel = NULL;
	int iTireCount = m_pWheels.Count();
	int a;
	for( a=0; a<iTireCount; a++ )
	{
		mksWheel = m_pWheels[a];
		if( mksWheel == NULL )
			continue;
		mksWheel->Update();
	}

	CalculateWheelData();
}


//-----------------------------------------------------
// CMKSKart::CalculateWheelData
//
// Description: Get the average of wheel values (Ground Normal, Velocity, etc.)
//-----------------------------------------------------
void CMKSKart::CalculateWheelData( void )
{
	Vector vNewGravity = Vector(0,0,1);
	Vector vCurrentGravity = GetGravityDirection();

	CMKSWheel *mksWheel = NULL;
	bool bIsOnGround = false;

	int iWheelCount = m_pWheels.Count();
	int iWheelGroundCount = 0;
	int a;
	for( a=0; a<iWheelCount; a++ )
	{
		mksWheel = GetWheel(a);
		if( mksWheel == NULL )
			continue;
		
		bIsOnGround = mksWheel->IsOnGround();

		vNewGravity += mksWheel->GetGroundNormal();

		if( bIsOnGround )
		{
			
			iWheelGroundCount++;
		}
	}

	SetWheelGroundCount( iWheelGroundCount );

	vNewGravity *= 100.0f;
	VectorNormalize( vNewGravity );
	//SetGravityDirection( -vNewGravity );
}


//-----------------------------------------------------
// CMKSKart::Update
//
// Description: Update the internal values
//-----------------------------------------------------
void CMKSKart::UpdateValues( void )
{
	IPhysicsObject *pKart = VPhysicsGetObject();

	pKart->GetPositionMatrix( &m_vMatWorld );
	MatrixVectors( m_vMatWorld, &m_vForward, &m_vRight, &m_vUp );

	Vector vOrigin = GetAbsOrigin();
	MatrixGetColumn( m_vMatWorld, 3, vOrigin );

	Vector vVelocity;
	AngularImpulse angVelocity;
	pKart->GetVelocity( &vVelocity, &angVelocity );

	float flKartVelocity = vVelocity.Length();
	SetKartVelocity( flKartVelocity );
	SetAbsVelocity( vVelocity );

	SetWorldOrigin( vOrigin );
}

//-----------------------------------------------------
// CMKSKart::Drive
//
// Description: Send the player input to the kart engine.
//-----------------------------------------------------
void CMKSKart::Drive( void )
{
	CMKSPlayer *mksPlayer = GetDriver();

	CUserCmd *pCmd = mksPlayer->GetCurrentUserCmd();
	GetEngine()->Steering( pCmd );
	GetEngine()->Gas( pCmd );
	GetEngine()->Brake( pCmd );
}


//-----------------------------------------------------
// CMKSKart::VPhysicsCollision
//
// Description: Detects Physics Collisions
//-----------------------------------------------------
void CMKSKart::VPhysicsCollision( int index, gamevcollisionevent_t *pEvent )
{
	int otherIndex = !index;
	CBaseEntity *pOther = pEvent->pEntities[otherIndex];
	if ( pOther->IsPlayer() )
	{
		// Pushables don't take damage from impacts with the player
		// We call all the way back to the baseclass to get the physics effects.
		CBaseEntity::VPhysicsCollision( index, pEvent );
		return;
	}

	Vector vNormal;
	Vector vPos = GetWorldOrigin();
	Vector vPoint;


	pEvent->pInternalData->GetSurfaceNormal( vNormal );
	pEvent->pInternalData->GetContactPoint( vPoint );

	Vector vDir = vPoint - vPos;
	VectorNormalize( vDir );
	float flDiff = DotProduct( vDir, vNormal );
	if( flDiff > 0.0f )
		vNormal *= -1;

	float flDot = DotProduct( GetGravityDirection(), vNormal );
	float flAbsDot = fabs(flDot);
	if( flAbsDot < 0.7f )
	{
		IPhysicsObject *pKart = VPhysicsGetObject();
		Vector vForce;
		pEvent->pInternalData->GetContactSpeed( vForce );
		float flSpeed = vForce.Length();
		pKart->ApplyForceCenter( vNormal * flSpeed * mks_wall_bounce.GetFloat() );
		SetKartSpeed( GetKartSpeed() * 0.5f );
	}

	BaseClass::VPhysicsCollision( index, pEvent );
}

//-----------------------------------------------------
// CMKSKart::InitProfile
//
// Description: Download the selected kart profile.
//-----------------------------------------------------
bool CMKSKart::InitProfile( void )
{
	CMKSPlayer *mksPlayer = GetDriver();

	const char* szKartName = mksPlayer->GetKartName();
	SetProfile( szKartName );

	PrecacheModel(m_pProfile.szModel);

	SetModel( m_pProfile.szModel );

	m_iWheelCount = m_pProfile.iWheelCount;

	int a;
	int iProfileCount = g_KartProfiles->m_Profiles.Count();
	MKSPlayerProfile *pProfile = NULL;

	for( a=0; a<iProfileCount; a++ )
	{
		pProfile = g_KartProfiles->m_Profiles[a];
		if( !pProfile )
			continue;
	
		const char *szModel = pProfile->szModel; 
		PrecacheModel( szModel );
		SetModel( szModel );

		GetProfileIndexes( pProfile );
	}
	return true;
}


//-----------------------------------------------------
// CMKSKart::InitializeKart
//
// Description: Setup any extra kart settings.
//-----------------------------------------------------
bool CMKSKart::InitializeKart( void )
{	
	InitializePhysics();
	SetPhysicsParams();

	//Build the kart engine.
	if( !GetEngine() )
	{
		CMKSEngine *pEngine = CMKSEngine::CreateEngine( this );
		if( pEngine == NULL )
			return false;
		SetEngine( pEngine );
	}

	//Build the kart wheels.
	CMKSWheel *pWheel = NULL;
	int a;
	for( a=0; a<m_iWheelCount; a++ )
	{
		pWheel = NULL;

		if( m_pWheels.Count() >= m_iWheelCount )
			pWheel = GetWheel(a);

		if( !pWheel )
		{
			pWheel = CMKSWheel::CreateWheel( this );
			if(!pWheel)
				continue;
		}
		pWheel->Init( &m_pProfile );
	}	

	if( !m_pMovement )
	{
		//Build the kart movement controller
		m_pMovement = CMKSKartMovement::CreateKartMovement( this );
		if( m_pMovement == NULL )
			return false;
	}

	

	return true;
}


//-----------------------------------------------------
// CMKSKart::InitializePhysics
//
// Description: Create the physics mesh of our model.
//-----------------------------------------------------
bool CMKSKart::InitializePhysics( void )
{
	bool bPassed = BaseClass::InitializePhysics();
	SetCollisionGroup( COLLISION_GROUP_MKS_KART );
	return bPassed;
}


//-----------------------------------------------------
// CMKSKart::CreateMinMax
//
// Description: Calculate the Min/Max of our karts model.
//-----------------------------------------------------
void CMKSKart::CreateMinMax( void )
{
	SetAbsOrigin( vec3_origin );
	SetAbsAngles( vec3_angle );

	InvalidateBoneCache();

	//Get the Min/Max of model
	const model_t *mod = GetModel();
	if ( !mod )
	{
		Warning("CMKSCube::CreateCube: GetModel failed for entity %i.\n", GetModelIndex() );
		return;
	}

	Vector mins, maxs;
	modelinfo->GetModelBounds( mod, mins, maxs );
	
	int iFrontRight = LookupAttachment( m_pProfile.szFrontLeft );
	int iRearRight = LookupAttachment( m_pProfile.szRearRight );

	Vector vFrontRight, vRearLeft;
	QAngle dummy;
	float flHeight = 25.0f;

	GetAttachmentLocal( iFrontRight, vFrontRight, dummy );
	GetAttachmentLocal( iRearRight, vRearLeft, dummy );

	mins = vRearLeft;

	maxs = vFrontRight;
	maxs.z = vFrontRight.z + flHeight;

	SetCollisionBounds( Vector(-0.5, -0.5, -0.5), Vector(0.5, 0.5, 0.5) );

	int iWheelCount = m_pProfile.iWheelCount;
	int a;
	float flMaxWheelRadius = 9999.9f;

	for( a=0; a<iWheelCount; a++ )
	{
		if( m_pProfile.flWheelRadius[a] < flMaxWheelRadius )
			flMaxWheelRadius = m_pProfile.flWheelRadius[a];
	}

	Vector vMinsDir = mins;
	Vector vMaxsDir = maxs;

	vMinsDir.z = 0;
	vMaxsDir.z = 0;

	VectorNormalize( vMinsDir );
	VectorNormalize( vMaxsDir );

	mins += vMinsDir * flMaxWheelRadius;
	maxs += vMaxsDir * flMaxWheelRadius;

	mins.z += -1.5f;

	m_vCubeMins = mins;
	m_vCubeMaxs = maxs;
}


//-----------------------------------------------------
// CMKSKart::SetPhysicsParams
//
// Description: Load the settings of our physics kart.
//-----------------------------------------------------
void CMKSKart::SetPhysicsParams( void )
{
	IPhysicsObject *pCube = VPhysicsGetObject();
	if(!pCube)
		return;

	float damping = m_flDamping = m_pProfile.flDamp;
	float rotdamping = m_flRotDamping = m_pProfile.flRotDamp;

	float drag = m_flDrag = m_pProfile.flDragCo;
	float rotdrag = m_flRotDrag = m_pProfile.flRotDrag;

	m_flMass = m_pProfile.flMass;
	m_flInertiaX = m_pProfile.vInertiaX;
	m_flInertiaY = m_pProfile.vInertiaY;
	m_flInertiaZ = m_pProfile.vInertiaZ;

	pCube->SetDamping( &damping, &rotdamping );
	pCube->SetDragCoefficient( &drag, &rotdrag );
	pCube->SetInertia( Vector( m_flInertiaX, m_flInertiaY, m_flInertiaZ ) );
	pCube->SetMass( m_flMass );

	pCube->EnableGravity( true );
	unsigned int contents = CONTENTS_PLAYERCLIP | CONTENTS_SOLID | CONTENTS_MOVEABLE;
	pCube->SetContents( contents );
	pCube->EnableCollisions( true );

	float flFriction = mks_friction_default.GetFloat();
	SetWheelFriction( flFriction );
}


//-----------------------------------------------------
// CMKSKart::CreateKart
//
// param1: Player who owns kart.
//
// Description: Create the kart and assign it to player.
//-----------------------------------------------------
CMKSKart *CMKSKart::CreateKart( CMKSPlayer *mksPlayer )
{
	if( !mksPlayer )
		return NULL;

	//Get position and EyeVectors (forward, right, up)
	Vector vOrigin = mksPlayer->GetAbsOrigin();
	Vector vForward, vUp, vRight;
	mksPlayer->EyeVectors( &vForward, &vRight, &vUp );

	//Create a point from origin through forward vector fDistance units.
	Vector vCubeOrigin = vOrigin;
	QAngle angAngle = QAngle(0,0,0);

	CMKSKart *mksKart = CREATE_ENTITY( CMKSKart, "mks_kart" );

	if ( !mksKart )
		return NULL;

	mksPlayer->SetOwnerEntity( mksKart );
	mksPlayer->SetParent( mksKart );
	//mksKart->SetOwnerEntity( mksPlayer );
	mksKart->SetDriver( mksPlayer );

	//Set Model Position
	//mksKart->SetAbsOrigin( vCubeOrigin );	
	//mksKart->SetSpawnOrigin( vCubeOrigin );
	mksKart->Spawn();
	//mksKart->SetAbsOrigin( vCubeOrigin );	

#ifdef DEBUG
	Warning( "Entity Created - \"mks_kart\" \n"  );
#endif


	angAngle = mksPlayer->GetAbsAngles();
	Vector vVelocity = Vector(0,0,0);

	
	

	IPhysicsObject *pKart = mksKart->VPhysicsGetObject();
	
	AngularImpulse angImpulse = AngularImpulse(0,0,0);

	vCubeOrigin += Vector(0,0,75);
	pKart->SetPosition( vCubeOrigin, angAngle, true );
	pKart->SetVelocityInstantaneous( &vVelocity, &angImpulse );

	//mksKart->Teleport( &vCubeOrigin, &angAngle, &vVelocity );
	return mksKart;
}