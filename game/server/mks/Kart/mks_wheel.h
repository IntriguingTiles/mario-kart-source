//--------------------------------------------------------------------------------------
//
//
//
//
//--------------------------------------------------------------------------------------

#ifndef MKS_WHEEL_H
#define MKS_WHEEL_H


#include "mks/Player/mks_player.h"

class CMKSKart;
class MKSPlayerProfile;

#define TRACE_DISTANCE_WHEEL_GROUND 125.0f

#ifndef DIST_EPSILON
	#define DIST_EPSILON (0.03125)
#endif

class CMKSWheel
{
public:

	CMKSWheel();
	~CMKSWheel();

	virtual void Update( void );
	virtual void UpdateValues( void );

	virtual void Init( MKSPlayerProfile *pProfile );
	
	virtual void DriveForce( void );
	virtual void Steering( void );
	virtual void Suspension( void );
	virtual void FindGround( void );

	virtual void Friction( void );

	static CMKSWheel *CreateWheel( CMKSKart *pOwner );


//Accessors
public:
	CMKSKart *GetKart() { return m_pKart; };
	void SetKart( CMKSKart *pKart ) { m_pKart = pKart; };

	CBaseEntity *GetGroundEntity() { return m_pGroundEntity; };
	void SetGroundEntity( CBaseEntity *pEnt ) { m_pGroundEntity = pEnt; };

	Vector GetGroundNormal() { return m_vGroundNormal; };
	void SetGroundNormal( Vector vNormal ) { m_vGroundNormal = vNormal; };

	Vector GetLocalHigh() { return m_vLocalHigh; };
	Vector GetLocalLow() { return m_vLocalLow; };

	Vector GetLocalOrigin() { return m_vLocalOrigin; };
	void SetLocalOrigin( Vector vOrigin ) { m_vLocalOrigin = vOrigin; };
	
	Vector GetWorldOrigin() { return m_vWorldOrigin; };
	void SetWorldOrigin( Vector vOrigin ) { m_vWorldOrigin = vOrigin; };
	
	Vector GetWheelVelocity() { return m_vWheelVelocity; };
	void SetWheelVelocity( Vector vVel ) { m_vWheelVelocity = vVel; };

	Vector GetDriveForce() { return m_vDriveForce; };
	void SetDriveForce( Vector vVel ) { m_vDriveForce = vVel; };

	Vector GetFrictionForce() { return m_vFrictionForce; };
	void SetFrictionForce( Vector vVel ) { m_vFrictionForce = vVel; };

	Vector GetSuspensionForce() { return m_vSuspensionForce; };
	void SetSuspensionForce( Vector vVel ) { m_vSuspensionForce = vVel; };

	bool IsOnGround() { return (m_flGroundDistance < GetRadius()); };
	//void SetIsOnGround( bool bGround ) { m_bIsOnGround = bGround; };

	bool IsNearGround() { return (m_flGroundDistance < TRACE_DISTANCE_WHEEL_GROUND); };
	//void SetIsNearGround( bool bGround ) { m_bIsNearGround = bGround; };

	bool IsDriveGround() { return (m_flGroundDistance < (GetRadius() + DIST_EPSILON*2.0f)); };
	//void SetIsDriveGround( bool bGround ) { m_bIsDriveGround = bGround; };

	float GetGroundDistance() { return m_flGroundDistance; };
	void SetGroundDistance( float flDist ) { m_flGroundDistance = flDist; };

	float GetRadius() { return m_flRadius; };
	void SetRadius( float flRadius ) { m_flRadius = flRadius; };

	float GetWidth() { return m_flWidth; };
	void SetWidth( float flWidth ) { m_flWidth = flWidth; };


//Variables
public:
	int m_iWheelId;

	int m_iAttachment;
	int m_iSuspension;

	float m_flSuspensionDistance;
	float m_flSuspensionPos;

	float m_flRadius;
	float m_flWidth;
	
	int m_iSteer;
	int m_iSpeed;

	bool m_bIsOnGround;
	bool m_bIsDriveGround;
	bool m_bIsNearGround;

	float m_flGroundDistance;

//Variables
private:
	CMKSKart *m_pKart;

	CBaseEntity *m_pGroundEntity;
	Vector m_vGroundNormal;

	Vector m_vLocalHigh;
	Vector m_vLocalLow;
	Vector m_vSpringDirection;

	Vector m_vLocalOrigin;
	Vector m_vWorldOrigin;

	Vector m_vWheelVelocity;
	
	Vector m_vDriveForce;
	Vector m_vFrictionForce;
	Vector m_vSuspensionForce;
};

#endif //MKS_WHEEL_H