//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Player for HL1.
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "mks_player.h"
#include "sdk_gamerules.h"
#include "viewport_panel_names.h"
#include "gamestats.h"
#include "obstacle_pushaway.h"
#include "in_buttons.h"

#include "engine/IEngineSound.h"
#include "rumble_shared.h"
#include "nav_mesh.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"


#include "mks/Kart/mks_kart.h"
#include "mks/Rules/mks_gamerules.h"


//it might make sense in the future to ues a array or somthing for player models
//#define	ENTITY_MODEL	"models/characters/playable/mario/mario.mdl"
#define	ENTITY_MODEL	"models/player/sdk_player_shared.mdl"
// -------------------------------------------------------------------------------- //
// Tables.
// -------------------------------------------------------------------------------- //

LINK_ENTITY_TO_CLASS( mks_player, CMKSPlayer );
PRECACHE_REGISTER(mks_player);


extern void SendProxy_Origin( const SendProp *pProp, const void *pStruct, const void *pData, DVariant *pOut, int iElement, int objectID );

BEGIN_SEND_TABLE_NOBASE( CMKSPlayer, DT_MKSLocalPlayerExclusive )	
END_SEND_TABLE()

BEGIN_SEND_TABLE_NOBASE( CMKSPlayer, DT_MKSNonLocalPlayerExclusive )
END_SEND_TABLE()


// main table
IMPLEMENT_SERVERCLASS_ST( CMKSPlayer, DT_MKSPlayer )
	// Data that only gets sent to the local player.
	SendPropDataTable( "mkslocaldata", 0, &REFERENCE_SEND_TABLE(DT_MKSLocalPlayerExclusive), SendProxy_SendLocalDataTable ),
	// Data that gets sent to all other players
	SendPropDataTable( "mksnonlocaldata", 0, &REFERENCE_SEND_TABLE(DT_MKSNonLocalPlayerExclusive), SendProxy_SendNonLocalDataTable ),
	SendPropEHandle(SENDINFO(m_hCurrentItem)),
	SendPropBool(SENDINFO(m_bHasItem)),
END_SEND_TABLE()

// -------------------------------------------------------------------------------- //


CMKSPlayer::CMKSPlayer() : CSDKPlayer()
{
}


CMKSPlayer::~CMKSPlayer()
{
	/*
	CMKSKart *mksKart = GetKart();
	if( mksKart )
	{
		mksKart->Remove();
	}
	*/
}


//This creates the actual CMKSPlayer class
CMKSPlayer *CMKSPlayer::CreatePlayer( const char *className, edict_t *ed )
{
	CMKSPlayer::s_PlayerEdict = ed;
	return ToMKSPlayer(CreateEntityByName( "mks_player" ));
}

void CMKSPlayer::Spawn( void )
{
	BaseClass::Spawn();

	/*
	// Needs to be done before weapons are given
	if ( Hints() )
	{
		Hints()->ResetHints();
	}
	//Tony; make sure tonemap params is cleared.
	//Tony; clear all the variables to -1.0
	m_Local.m_TonemapParams.m_flAutoExposureMin = -1.0f;
	m_Local.m_TonemapParams.m_flAutoExposureMax = -1.0f;
	m_Local.m_TonemapParams.m_flTonemapScale = -1.0f;
	m_Local.m_TonemapParams.m_flBloomScale = -1.0f;
	m_Local.m_TonemapParams.m_flTonemapRate = -1.0f;

	SetClassname( "player" );
	
	SetSimulatedEveryTick( true );
	SetAnimatedEveryTick( true );

	SetBlocksLOS( false );
	m_iMaxHealth		= m_iHealth;

	// Clear all flags except for FL_FULLEDICT
	if ( GetFlags() & FL_FAKECLIENT )
	{
		ClearFlags();
		AddFlag( FL_CLIENT | FL_FAKECLIENT );
	}
	else
	{
		ClearFlags();
		AddFlag( FL_CLIENT );
	}

 // only preserve the shadow flag
	int effects = GetEffects() & EF_NOSHADOW;
	SetEffects( effects | EF_NOINTERP );

	// Initialize the fog controller.
	InitFogController();

	m_afPhysicsFlags	= 0;


	SetFOV( this, 0 );

	
	m_flFieldOfView		= 0.766;// some NPCs use this to determine whether or not the player is looking at them.

	m_vecAdditionalPVSOrigin = vec3_origin;
	m_vecCameraPVSOrigin = vec3_origin;

	
	Precache();
	
	m_bitsDamageType = 0;
	m_bitsHUDDamage = -1;
	SetPlayerUnderwater( false );

	SetThink(NULL);

	
	m_lastNavArea = NULL;

#ifndef _XBOX
	/// @todo Do this once per round instead of once per player
	if (TheNavMesh)
	{
		TheNavMesh->ClearPlayerCounts();
	}
#endif

	Q_strncpy( m_szLastPlaceName.GetForModify(), "", MAX_PLACE_NAME_LENGTH );
	
	CSingleUserRecipientFilter user( this );
	enginesound->SetPlayerDSP( user, 0, false );


	if ( GetTeamNumber() != TEAM_SPECTATOR )
	{
		StopObserverMode();
	}
	else
	{
		StartObserverMode( m_iObserverLastMode );
	}

	StopReplayMode();

	// Clear any screenfade
	color32 nothing = {0,0,0,255};
	UTIL_ScreenFade( this, nothing, 0, 0, FFADE_IN | FFADE_PURGE );

	g_pGameRules->PlayerSpawn( this );

	IGameEvent *event = gameeventmanager->CreateEvent( "player_spawn" );
	if ( event )
	{
		event->SetInt("userid", GetUserID() );
		gameeventmanager->FireEvent( event );
	}

	RumbleEffect( RUMBLE_STOP_ALL, 0, RUMBLE_FLAGS_NONE );


	SetModel( "models/player/blue_player.mdl" );	//Tony; basically, leave this alone ;) unless you're not using classes or teams, then you can change it to whatever.
	
	SetBloodColor( BLOOD_COLOR_RED );
	
	
	RemoveSolidFlags( FSOLID_NOT_SOLID );
	*/

	//SetCollisionGroup( COLLISION_GROUP_MKS_PLAYER );

	//if( State_Get() == STATE_ACTIVE )
	{
		SetKart( CMKSKart::CreateKart( this ) );

		//SetSolid( SOLID_NONE );
		//SetMoveType( MOVETYPE_NONE );

		//AddEffects( EF_NODRAW );
	}
	SetSpawnLocation();

	SetModel(ENTITY_MODEL);

	//SetMoveType( MOVETYPE_WALK );
	//SetSolid( SOLID_BBOX );
	SetMoveType( MOVETYPE_NONE );
	SetSolid( SOLID_NONE );

	pl.deadflag = false;
	m_lifeState	= LIFE_ALIVE;
	m_iHealth = 100;
	m_takedamage		= DAMAGE_YES;

	ShowViewModel(false);
	m_hCurrentItem = 0;
	m_bWasAttackDown = false;
}

void CMKSPlayer::SetSpawnLocation( void ) {
	CBaseEntity *pSpawnSpot = g_pGameRules->GetPlayerSpawnSpot( this );


	// drop down to ground
	Vector vOrigin = pSpawnSpot->GetAbsOrigin();
	QAngle qAngle = pSpawnSpot->GetAbsAngles();

	

	Vector vMins = WorldAlignMins();
	Vector vMaxs = WorldAlignMaxs();

	CMKSKart *mksKart = GetKart();
	if( mksKart ) {
		vMins = mksKart->m_vCubeMins;
		vMaxs = mksKart->m_vCubeMaxs;
	}
	trace_t trace;
	UTIL_TraceHull( vOrigin, vOrigin - Vector( 0, 0, 500 ), vMins, vMaxs, MASK_SOLID, this, COLLISION_GROUP_NONE, &trace );

	vOrigin.z = trace.endpos.z + 25.0f;
	// Move the player to the place it said.
	SetLocalOrigin( vOrigin );
	//SetLocalOrigin( pSpawnSpot->GetAbsOrigin() + Vector(0,0,1) );
	SetAbsVelocity( vec3_origin );
	SetLocalAngles( pSpawnSpot->GetLocalAngles() );
	m_Local.m_vecPunchAngle = vec3_angle;
	m_Local.m_vecPunchAngleVel = vec3_angle;
	SnapEyeAngles( pSpawnSpot->GetLocalAngles() );

	//Teleport( &vOrigin, &pSpawnSpot->GetLocalAngles(), &vec3_origin );
	m_Local.m_vecPunchAngle = vec3_angle;
}

void CMKSPlayer::InitialSpawn( void )
{
	CBasePlayer::InitialSpawn( );

	// player just closed MOTD dialog
	// Tony; using teams, go to picking team.
	//State_Transition( STATE_PICKINGTEAM );

	//State_Transition( STATE_ACTIVE );
}

void CMKSPlayer::PlayerRunCommand(CUserCmd *ucmd, IMoveHelper *moveHelper)
{
	CUserCmd pCommand = *ucmd;

	SetCurrentUserCmd( &pCommand );

	BaseClass::PlayerRunCommand( ucmd, moveHelper );

	CMKSKart *mksKart = GetKart();
	if( !mksKart )
		return;

	Vector vOrigin = mksKart->GetAbsOrigin();
	SetAbsOrigin( vOrigin );
}

void CMKSPlayer::Precache( void )
{
	PrecacheModel( ENTITY_MODEL );
}

bool CMKSPlayer::ClientCommand( const CCommand &args )
{
	return BaseClass::ClientCommand( args );
}

IPhysicsObject *CMKSPlayer::GetPhysKart() {
	CMKSKart *mksKart = GetKart();
	if( !mksKart )
		return NULL;
	return mksKart->VPhysicsGetObject();
}

void CMKSPlayer::CalcView( Vector &eyeOrigin, QAngle &eyeAngles, float &zNear, float &zFar, float &fov )
{
	BaseClass::CalcView( eyeOrigin, eyeAngles, zNear, zFar, fov );
}

void CMKSPlayer::PostThink()
{
	BaseClass::PostThink();

	if (!m_hCurrentItem)
		return;
	if (m_hCurrentItem->IsItemDone()){
		UTIL_Remove(m_hCurrentItem);
		m_hCurrentItem = 0;
		m_bHasItem = false;
		return;
	}
	if (m_nButtons & IN_ATTACK){
		if (!m_bWasAttackDown){
			m_hCurrentItem->AttackDown();
		}
		m_bWasAttackDown = true;
	}else{
		if (m_bWasAttackDown){
			m_hCurrentItem->AttackUp();
		}
		m_bWasAttackDown = false;
	}
}


bool CMKSPlayer::SelectSpawnSpot( const char *pEntClassName, CBaseEntity* &pSpot )
{
	// Find the next spawn spot.
	pSpot = gEntList.FindEntityByClassname( pSpot, pEntClassName );

	if ( pSpot == NULL ) // skip over the null point
		pSpot = gEntList.FindEntityByClassname( pSpot, pEntClassName );

	CBaseEntity *pFirstSpot = pSpot;
	do 
	{
		if ( pSpot )
		{
			// check if pSpot is valid
			if ( g_pGameRules->IsSpawnPointValid( pSpot, this ) )
			{
				if ( pSpot->GetAbsOrigin() == Vector( 0, 0, 0 ) )
				{
					pSpot = gEntList.FindEntityByClassname( pSpot, pEntClassName );
					continue;
				}

				// if so, go to pSpot
				return true;
			}
		}
		// increment pSpot
		pSpot = gEntList.FindEntityByClassname( pSpot, pEntClassName );
	} while ( pSpot != pFirstSpot ); // loop if we're not back to the start

	DevMsg("CSDKPlayer::SelectSpawnSpot: couldn't find valid spawn point.\n");

	return true;
}


EHANDLE g_pMKSLastBlueSpawn;
EHANDLE g_pMKSLastRedSpawn;
EHANDLE g_pMKSLastUnknown;

LINK_ENTITY_TO_CLASS( mks_player_spawn, CPointEntity );


CBaseEntity* CMKSPlayer::EntSelectSpawnPoint()
{
	CBaseEntity *pSpot = NULL;

	const char *pSpawnPointName = "";

	switch( GetTeamNumber() )
	{
#if defined ( SDK_USE_TEAMS )
	case SDK_TEAM_BLUE:
		{
			pSpawnPointName = "mks_player_spawn";
			pSpot = g_pMKSLastBlueSpawn;
			if ( SelectSpawnSpot( pSpawnPointName, pSpot ) )
			{
				g_pMKSLastBlueSpawn = pSpot;
			}
		}
		break;
	case SDK_TEAM_RED:
		{
			pSpawnPointName = "mks_player_spawn";
			pSpot = g_pMKSLastRedSpawn;
			if ( SelectSpawnSpot( pSpawnPointName, pSpot ) )
			{
				g_pMKSLastRedSpawn = pSpot;
			}
		}		
		break;
#endif // SDK_USE_TEAMS
	case TEAM_UNASSIGNED:
		{
			pSpawnPointName = "mks_player_spawn";
			pSpot = g_pMKSLastBlueSpawn;
			if ( SelectSpawnSpot( pSpawnPointName, pSpot ) )
			{
				g_pMKSLastBlueSpawn = pSpot;
			}
		}
		break;
	case TEAM_SPECTATOR:
	default:
		{
			pSpot = CBaseEntity::Instance( INDEXENT(0) );
		}
		break;		
	}

	if ( !pSpot )
	{
		Warning( "PutClientInServer: no %s on level\n", pSpawnPointName );
		return CBaseEntity::Instance( INDEXENT(0) );
	}

	return pSpot;
} 
