//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Player for SDK Game
//
// $NoKeywords: $
//=============================================================================//

#ifndef MKS_PLAYER_H
#define MKS_PLAYER_H
#pragma once

#include "sdk_player.h"
#include "mks/Items/mks_item.h"

class CMKSKart;

//=============================================================================
// >> MKSPlayer
//=============================================================================
class CMKSPlayer : public CSDKPlayer
{
public:
	DECLARE_CLASS( CMKSPlayer, CSDKPlayer );
	DECLARE_SERVERCLASS();
	DECLARE_PREDICTABLE();

	CMKSPlayer();
	~CMKSPlayer();

	static CMKSPlayer *CreatePlayer( const char *className, edict_t *ed );
	static CMKSPlayer* Instance( int iEnt );

public:
	virtual void Spawn( void );
	virtual void InitialSpawn(  void );

	virtual void CalcView( Vector &eyeOrigin, QAngle &eyeAngles, float &zNear, float &zFar, float &fov );
	virtual void PlayerRunCommand(CUserCmd *ucmd, IMoveHelper *moveHelper);

	CBaseEntity*	EntSelectSpawnPoint();
	bool SelectSpawnSpot( const char *pEntClassName, CBaseEntity* &pSpot );
	virtual void SetSpawnLocation( void );

	virtual void PostThink();
	void Precache();
public:
	CMKSKart *GetKart() { return m_pKart; };
	void SetKart( CMKSKart *pKart ) { m_pKart = pKart; };

	IPhysicsObject *GetPhysKart();

	const char *GetKartName() { return "Default"; };
	void SetKartName( const char *szKartName ) { m_szKartName = (char *)szKartName; };

	CUserCmd *GetCurrentUserCmd() { return &m_pCurrentCmd; };
	void SetCurrentUserCmd( CUserCmd *pCmd ) { m_pCurrentCmd = *pCmd; };

	virtual bool ClientCommand( const CCommand &args );
	

	bool HasItem() {return m_bHasItem;}
	CMKSItem *GetCurrentItem() {return m_hCurrentItem;}
	void SetCurrentItem(CMKSItem *item) {m_hCurrentItem = item; m_bHasItem = true;}
	

private:
	char *m_szKartName;
	CUserCmd m_pCurrentCmd;

	CMKSKart *m_pKart;
	CNetworkVar(bool, m_bHasItem);
	CNetworkHandle( CMKSItem, m_hCurrentItem );

	bool m_bWasAttackDown;
};


//=============================================================================
// >> Convert Entity to MKSPlayer
//=============================================================================
inline CMKSPlayer *ToMKSPlayer( CBaseEntity *pEntity )
{
	if ( !pEntity || !pEntity->IsPlayer() )
		return NULL;

#ifdef _DEBUG
	Assert( dynamic_cast<CMKSPlayer*>( pEntity ) != 0 );
#endif
	return static_cast< CMKSPlayer* >( pEntity );
}

#endif	// MKS_PLAYER_H
