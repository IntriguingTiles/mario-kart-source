#include "cbase.h"
#include "mks_item.h"

static const char * s_ItemEntityNames[] = 
{
	"none",		// ITEM_NONE
	"mks_item_green_shell",
	NULL,		// ITEM_NONE
};


CMKSItem *CreateItem(MKSItemID id, CMKSPlayer *pOwner)
{

	CMKSItem *pItem = dynamic_cast<CMKSItem *>(CreateEntityByName(s_ItemEntityNames[id]));
	pItem->SetOwner(pOwner);
	return pItem;
}

IMPLEMENT_NETWORKCLASS_DT( CMKSItem, DT_CMKSItem )
END_NETWORK_TABLE()