#pragma once

class CMKSItem;
class CMKSPlayer;

typedef enum
{
	ITEM_NONE = 0,
	ITEM_GREEN_SHELL,
	
	ITEM_MAX,		// number of weapons weapon index
} MKSItemID;

CMKSItem *CreateItem(MKSItemID id, CMKSPlayer *pOwner);

class CMKSItem : public CBaseEntity
{
public:
	DECLARE_CLASS( CMKSItem, CBaseEntity );
	DECLARE_NETWORKCLASS();
	virtual MKSItemID GetItemId() {return ITEM_NONE;}
	virtual void AttackDown() {};
	virtual void AttackUp() {};
	void SetOwner(CMKSPlayer *pOwner) {m_pOwner = pOwner;}
	virtual bool IsItemDone() {return true;}
protected:
	CMKSPlayer *m_pOwner;
};
