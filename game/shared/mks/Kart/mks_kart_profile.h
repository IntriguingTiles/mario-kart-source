#ifndef MKS_KART_PROFILE_H
#define MKS_KART_PROFILE_H

#ifdef CLIENT_DLL
#define CMKSWheel C_MKSWheel
#define CMKSPlayer C_MKSPlayer
#endif

class CMKSWheel;

#define MKS_MAX_WHEELS			8
#define MKS_MAX_STRING_NAME		128

#ifdef CLIENT_DLL
EXTERN_RECV_TABLE( DT_MKSKartProfileInfo );
#else
EXTERN_SEND_TABLE( DT_MKSKartProfileInfo );
#endif

class MKSPlayerProfile
{
	DECLARE_CLASS_NOBASE( MKSPlayerProfile );
	DECLARE_EMBEDDED_NETWORKVAR();

public:
	void Copy( MKSPlayerProfile *fromProfile );

	// 3d skybox camera data
	CNetworkVar( int, id );

	CNetworkString( szName, MKS_MAX_STRING_NAME );
	CNetworkString( szModel, MKS_MAX_STRING_NAME );
	
	CNetworkString( szFrontLeft, MKS_MAX_STRING_NAME );
	CNetworkString( szRearRight, MKS_MAX_STRING_NAME );

	CNetworkVar( float, flDamp );
	CNetworkVar( float, flDragCo );
	CNetworkVar( float, flMass );
	CNetworkVar( float, flRotDamp );
	CNetworkVar( float, flRotDrag );
	CNetworkVar( float, flRotInertia );

	CNetworkVar( float, vInertiaX );
	CNetworkVar( float, vInertiaY );
	CNetworkVar( float, vInertiaZ );

	CNetworkVar( int, iWheelCount );

	char szWheelAttachment[MKS_MAX_WHEELS][MKS_MAX_STRING_NAME];
	char szWheelSuspension[MKS_MAX_WHEELS][MKS_MAX_STRING_NAME];

	CNetworkArray( int, iWheelSuspension, MKS_MAX_WHEELS );
	CNetworkArray( int, iWheelAttachment, MKS_MAX_WHEELS );
	CNetworkArray( float, flWheelRadius, MKS_MAX_WHEELS );
	CNetworkArray( float, flWheelWidth, MKS_MAX_WHEELS );
	CNetworkArray( int, iWheelSpeed, MKS_MAX_WHEELS );
	CNetworkArray( int, iWheelSteer, MKS_MAX_WHEELS );
};


class MKSKartProfile
{
public:
	MKSKartProfile();
	~MKSKartProfile();

public:

	void ReloadProfiles();
	void LoadProfiles();
	void ParseProfile( KeyValues *kProfile );

	MKSPlayerProfile *GetProfile( const char *szName );

public:
	
	CUtlVector< MKSPlayerProfile* > m_Profiles;
};

extern MKSKartProfile *g_KartProfiles;

#endif //MKS_KART_PROFILE_H