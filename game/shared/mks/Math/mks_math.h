#ifndef MKS_MATH_H
#define MKS_MATH_H

float ConvergeOrigin( float flStart, float flEnd, float flUnitsPerSecond );

int ClipVelocity( Vector& in, Vector& normal, Vector& out, float overbounce );


#endif //MKS_MATH_H