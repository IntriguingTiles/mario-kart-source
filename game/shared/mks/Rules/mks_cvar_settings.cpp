#include "cbase.h"
#include "mks/Rules/mks_cvar_settings.h"

ConVar mks_spring_stiff( "mks_spring_stiff", "70000", FCVAR_REPLICATED );
ConVar mks_spring_damping( "mks_spring_damping", "1450", FCVAR_REPLICATED );

ConVar mks_friction_default( "mks_friction_default", "2", FCVAR_REPLICATED );
ConVar mks_friction_slide( "mks_friction_slide", "0.2", FCVAR_REPLICATED );

ConVar mks_friction_multiplier( "mks_friction_multiplier", "1", FCVAR_REPLICATED );

ConVar mks_upright_ground( "mks_upright_ground", "200.0", FCVAR_REPLICATED );

ConVar mks_forward_speed( "mks_forward_speed", "100.0", FCVAR_REPLICATED );
ConVar mks_forward_maxspeed( "mks_forward_maxspeed", "150.0", FCVAR_REPLICATED );
ConVar mks_reverse_speed( "mks_reverse_speed", "100.0", FCVAR_REPLICATED );
ConVar mks_reverse_maxspeed( "mks_reverse_maxspeed", "100.0", FCVAR_REPLICATED );
ConVar mks_neutral_decayspeed( "mks_neutral_decayspeed", "50.0", FCVAR_REPLICATED );

ConVar mks_jump_speed( "mks_jump_speed", "15000", FCVAR_REPLICATED );

ConVar mks_steering_min( "mks_steering_min", "-40.0", FCVAR_REPLICATED );
ConVar mks_steering_max( "mks_steering_max", "40.0", FCVAR_REPLICATED );
ConVar mks_steering_speed( "mks_steering_speed", "300.0", FCVAR_REPLICATED );

ConVar mks_wall_bounce( "mks_wall_bounce", "7.0", FCVAR_REPLICATED );

extern ConVar mks_forward_speed;
extern ConVar mks_forward_maxspeed;
extern ConVar mks_reverse_speed;
extern ConVar mks_reverse_maxspeed;
extern ConVar mks_neutral_speed;
extern ConVar mks_neutral_maxspeed;

/*
ConVar mks_wheel_radius( "mks_wheel_radius", "10", FCVAR_REPLICATED );
ConVar mks_wheel_rotDrag( "mks_wheel_rotDrag", "2.0", FCVAR_REPLICATED );
ConVar mks_wheel_mass( "mks_wheel_mass", "2.0", FCVAR_REPLICATED );
ConVar mks_wheel_rotDamp( "mks_wheel_rotDamp", "1.0", FCVAR_REPLICATED );
ConVar mks_wheel_rotIner( "mks_wheel_rotIner", "1.0", FCVAR_REPLICATED );
ConVar mks_wheel_inertiax( "mks_wheel_inertiax", "2.5", FCVAR_REPLICATED );
ConVar mks_wheel_inertiay( "mks_wheel_inertiay", "2.5", FCVAR_REPLICATED );
ConVar mks_wheel_inertiaz( "mks_wheel_inertiaz", "2.5", FCVAR_REPLICATED );
ConVar mks_wheel_forward_dist( "mks_wheel_forward_dist", "22", FCVAR_REPLICATED );
ConVar mks_wheel_side_dist( "mks_wheel_side_dist", "22", FCVAR_REPLICATED );
ConVar mks_wheel_up_dist( "mks_wheel_up_dist", "10", FCVAR_REPLICATED );
ConVar mks_wheel_scale( "mks_wheel_scale", "2", FCVAR_REPLICATED );
ConVar mks_wheel_elastic( "mks_wheel_elastic", "0.3", FCVAR_REPLICATED );
ConVar mks_wheel_rightoffset( "mks_wheel_rightoffset", "0.0", FCVAR_REPLICATED );
ConVar mks_wheel_forwardoffset( "mks_wheel_forwardoffset", "0.5", FCVAR_REPLICATED );


ConVar mks_body_masscenter( "mks_body_masscenter", "0.0", FCVAR_REPLICATED );


ConVar mks_spawndist( "mks_spawndist", "500", FCVAR_REPLICATED );
ConVar mks_move_forward( "mks_move_forward", "350", FCVAR_REPLICATED );
ConVar mks_move_side( "mks_move_side", "20", FCVAR_REPLICATED );
ConVar mks_move_turn( "mks_move_turn", "800", FCVAR_REPLICATED );
ConVar mks_move_reverse( "mks_move_reverse", "-350", FCVAR_REPLICATED );
ConVar mks_move_maxreverse( "mks_move_maxreverse", "-300", FCVAR_REPLICATED );
ConVar mks_move_maxforward( "mks_move_maxforward", "700", FCVAR_REPLICATED );
ConVar mks_move_airtime( "mks_move_airtime", "1.0", FCVAR_REPLICATED );

ConVar mks_jump_delay( "mks_jump_delay", "0.3", FCVAR_REPLICATED );
ConVar mks_jump_speed( "mks_jump_speed", "100", FCVAR_REPLICATED );
ConVar mks_jump_max( "mks_jump_max", "100.0", FCVAR_REPLICATED );

ConVar mks_lerp_speed( "mks_lerp_speed", "50", FCVAR_REPLICATED );
ConVar mks_origin_speed( "mks_origin_speed", "1800", FCVAR_REPLICATED );
ConVar mks_origin_maxdist( "mks_origin_maxdist", "100", FCVAR_REPLICATED );
ConVar mks_bounce( "mks_bounce", "200", FCVAR_REPLICATED );

ConVar mks_traction_converge( "mks_traction_converge", "25", FCVAR_REPLICATED );



ConVar mks_upright_air_max( "mks_upright_air_max", "0.0", FCVAR_REPLICATED );
ConVar mks_upright_air_min( "mks_upright_air_min", "0.0", FCVAR_REPLICATED );

ConVar mks_upright_sticky( "mks_upright_sticky", "100.0", FCVAR_REPLICATED );
ConVar mks_upright_floating( "mks_upright_floating", "100.0", FCVAR_REPLICATED );


ConVar mks_gravity_air_speed( "mks_gravity_air_speed", "800.0", FCVAR_REPLICATED );
ConVar mks_gravity_air_max( "mks_gravity_air_max", "2000.0", FCVAR_REPLICATED );
ConVar mks_gravity_ground_speed( "mks_gravity_ground_speed", "800.0", FCVAR_REPLICATED );
ConVar mks_gravity_ground_max( "mks_gravity_ground_max", "50.0", FCVAR_REPLICATED );
ConVar mks_gravity_default_speed( "mks_gravity_default_speed", "800.0", FCVAR_REPLICATED );
ConVar mks_gravity_default_max( "mks_gravity_default_max", "2000.0", FCVAR_REPLICATED );


ConVar mks_bounceSpring_stiff( "mks_bounceSpring_stiff", "1.0", FCVAR_REPLICATED );
ConVar mks_bounceSpring_damp( "mks_bounceSpring_damp", "1.0", FCVAR_REPLICATED );


ConVar mks_prediction_distance_error( "mks_prediction_distance_error", "100.0", FCVAR_REPLICATED );

ConVar mks_fakelag( "mks_fakelag", "0.0", FCVAR_REPLICATED );
//ConVar mks_player_size( "mks_player_size", "0.5" );

ConVar mks_debug_draw( "mks_debug_draw", "1", FCVAR_REPLICATED );
*/